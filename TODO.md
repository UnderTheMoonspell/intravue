# TODO - IntraVue

## General

* ~~Passar FullCalendar e lista (periodos, membros) para um componente;~~


## Search

* Componentes de listagem;
* Incluír filtros especificos;


## User

#### Mailing Lists

* Testar com dados reais;

#### Projects

* Imagens nos projetos;

#### Tickets

* Concluír _bind_ dos dados da tabela (precisa de dados);

#### Ausências

* Rever funções: `isDayHoliday` e `removeWeekendsAndHolidays` (precisa de dados de feriádos);
* ~~Rever traduções do calendário (moment & fullcalendar);~~
* ~~Semana a começar na 2ªf (moment.locale);~~
* ~~Delete Icon nos eventos, ou no ultimo dia do grupo de eventos (css: hover);~~
* ~~Ocultar + (plus) se o dia já foi marcado como férias ou feriado, ocultar dos fins de semana;~~
* ~~Ao selecionar o grupo (periodos), selecciona os dias no calendário;~~
* ~~Permitir extender dias de férias, apenas para a frente (draggable, editable, FCDocs);~~
* ~~Cor do calendário vêm do user (app config);~~

#### Configurações

* Ao alterar de idioma, atualizar as labels;
* ~~Seleccionar idioma activo;~~