# intra-vue

O form input para nao ter que estar sempre a passar os eventos para cima podia ficar so
(isto funciona mas nao eh uma boa pratica - https://forum.vuejs.org/t/v-model-on-prop/37639/9)

  <FormInput
    :class-names="'control-group all-50 validation'"
    :disabled="readOnly"
    :type="'text'"
    :input-value="resource.name"
    :label="'Name'"
    :name="'name'"
    v-model="resource.name"
    :validations="{ min_value: 0, max_value: 9 }">
  </FormInput>

  model: {
      prop: 'inputValue',
      event: 'input'
  },

  <input :type="type"
      :disabled="disabled"
      :value="inputValue"
      @input="$emit('input', $event.target.value)"
      :maxlength="maxLength"
      :name="name"
      :autofocus="autoFocus"
      :placeholder="placeholder">          

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your unit tests
```
npm run test:unit
```
