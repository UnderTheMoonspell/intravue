export default {
    vacations: state => state.vacations,
    holidays: state => state.holidays,
    events: state => state.events,
    eventsFilters: state => state.eventsFilters,
    error: state => state.error,
    isLoading: state => state.isLoading
}

