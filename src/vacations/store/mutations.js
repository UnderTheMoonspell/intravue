import types from "@/mutation.types";

export default {

    [types.VACATIONS.GET_USER_VACATIONS_SUCCESS](state, obj) {
        state.vacations = obj;
        state.isLoading = false;
    },

    [types.VACATIONS.GET_HOLIDAYS_SUCCESS](state, obj) {
        state.holidays = obj;
        state.isLoading = false;
    },

    [types.VACATIONS.EDIT_USER_VACATIONS_SUCCESS](state, obj) {
        state.isLoading = false;
    },

    [types.LOADING](state) {
        state.isLoading = true;
    },

    [types.ERROR](state, obj) {
        state.error = obj;
    }
};
