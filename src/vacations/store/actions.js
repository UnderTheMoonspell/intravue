import DataService from "@/services/data.service";
import appConfig from "@/app.config";
import types from "@/mutation.types";

export default {

    async [types.VACATIONS.GET_USER_VACATIONS]({ commit }, { user, start, end, inactive }) {
        try {
            let vacations = await DataService.get(appConfig.APIURLs.getUserVacations({ user, start, end, inactive }));
            commit(types.VACATIONS.GET_USER_VACATIONS_SUCCESS, vacations.sort((a, b) => {
                return new Date(a[appConfig.calendar.api_key]).getTime() - new Date(b[appConfig.calendar.api_key]).getTime();
            }));
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.VACATIONS.GET_HOLIDAYS]({ commit }, { since, until }) {
        try {
            let holidays = await DataService.get(appConfig.APIURLs.getHolidays({ since, until }));
            commit(types.VACATIONS.GET_HOLIDAYS_SUCCESS, holidays);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.VACATIONS.EDIT_USER_VACATIONS]({ commit, dispatch }, data) {
        try {
            await DataService.post(appConfig.APIURLs.editUserVacations(data.user), data.payload);
            dispatch(types.VACATIONS.GET_USER_VACATIONS, data);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    }
};
