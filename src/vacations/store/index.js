import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import appConfig from "@/app.config";
import { createNamespacedHelpers } from "vuex";

const state = {
  vacations: [],
  holidays: [],
  events: [],
  eventsFilters: [],
  isLoading: false,
  error: null
};

export const { mapGetters, mapActions } = createNamespacedHelpers(`${appConfig.storeNamespace.vacations}`);

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
