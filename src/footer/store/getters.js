export default {
    pages: state => state.pages,
    isLoading: state => state.isLoading,
    helpTOCId: state => state.helpTOCId
}

