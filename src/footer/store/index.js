import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import appConfig from "@/app.config";
import { createNamespacedHelpers } from "vuex";

const state = {
    pages: [],
    isLoading: false,
    helpTOCId: "",
    error: null
};

export const { mapGetters, mapActions, mapMutations } = createNamespacedHelpers(
    `${appConfig.storeNamespace.footer}`
);

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
};
