import DataService from "@/services/data.service";
import appConfig from "@/app.config";
import types from "@/mutation.types";

export default {
    async [types.FOOTER.GET_HELP]({ commit }, { context }) {
        try {
            commit(types.LOADING);
            // TODO ESTE ENDPOINT PRECISA DO SOLR A CORRER
            let pages = await DataService.get(appConfig.APIURLs.getFooterHelp(context));
            commit(types.FOOTER.GET_HELP_SUCCESS, pages);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    }
};