import types from "@/mutation.types";

export default {
    [types.FOOTER.GET_HELP_SUCCESS](state, obj) {
        state.pages = obj;
        state.isLoading = false;
    },

    [types.LOADING](state) {
        state.isLoading = true;
    },

    [types.ERROR](state, obj) {
        state.error = obj;
        state.isLoading = false;
    },

    [types.FOOTER.SET_HELP_ID](state, obj) {
        state.helpTOCId = obj;
    }
};
