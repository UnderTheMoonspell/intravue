import types from "@/mutation.types";
import DataService from "@/services/data.service";
import appConfig from "@/app.config";

export default {
    [types.GET_SUCCESS](state, obj) {
        state.tasks = obj;
    },

    [types.TASKS.GET_SUCCESS_TOP](state, obj) {
        state.topTasks = obj;
    },

    [types.TASKS.GET_FILTERS_SUCCESS](state, obj) {
        state.filters = obj;
    },

    [types.ERROR](state, obj) {
        state.error = obj;
    },

    [types.INIT_APP](state) {
        let defaultFilter = DataService.getFromLocal(appConfig.localStorageKeys.tasksFilters);
        state.defaultFilter = defaultFilter || 'all';
    },

    [types.TASKS.SET_WIDGET_FILTER](state, obj) {
        DataService.setLocal(appConfig.localStorageKeys.tasksFilters, obj);
        state.defaultFilter = obj;
    }
};
