import DataService from "@/services/data.service";
import appConfig from "@/app.config";
import types from "@/mutation.types";

export default {
    async [types.GET]({ commit }, { user, filter }) {
        try {
            let tasks = await DataService.get(appConfig.APIURLs.getTasks(user, filter));
            commit(types.GET_SUCCESS, tasks);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.TASKS.GET_TOP]({ commit }) {
        try {
            let tasks = await DataService.get(appConfig.APIURLs.getTopTasks);
            commit(types.TASKS.GET_SUCCESS_TOP, tasks);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.TASKS.GET_FILTERS]({ commit }, { user }) {
        try {
            let filters = await DataService.get(appConfig.APIURLs.getTasksFilters(user));
            commit(types.TASKS.GET_FILTERS_SUCCESS, filters);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    }
};
