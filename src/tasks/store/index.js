import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import appConfig from "@/app.config";
import { createNamespacedHelpers } from "vuex";

const state = {
  tasks: [],
  topTasks: [],
  filters: [],
  defaultFilter: null,
  error: null
};

// localStorage.getItem('taskFilter') || 'all',
export const { mapGetters, mapActions, mapMutations } = createNamespacedHelpers(`${appConfig.storeNamespace.tasks}`);

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
