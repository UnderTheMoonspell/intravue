export default {
    tasks: state => state.tasks,
    topTasks: state => state.topTasks,
    filters: state => state.filters,
    error: state => state.error,
    defaultFilter: state => state.defaultFilter
}