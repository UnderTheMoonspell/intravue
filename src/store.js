import Vue from 'vue'
import Vuex from 'vuex'
import userStore from '@/user/store';
import appConfig from '@/app.config';
import barStore from '@/intra-bar/store';
import ticketsStore from '@/tickets/store';
import tasksStore from '@/tasks/store';
import projectsStore from '@/projects/store';
import dashboardStore from '@/dashboard/store';
import footerStore from '@/footer/store';
import layoutStore from '@/layout/store';
import mailingListsStore from '@/mailing-lists/store';
import vacationsStore from '@/vacations/store';
import searchStore from '@/search/store';

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  modules: {
    [appConfig.storeNamespace.user]: userStore,
    [appConfig.storeNamespace.intrabar]: barStore,
    [appConfig.storeNamespace.tickets]: ticketsStore,
    [appConfig.storeNamespace.tasks]: tasksStore,
    [appConfig.storeNamespace.projects]: projectsStore,
    [appConfig.storeNamespace.dashboard]: dashboardStore,
    [appConfig.storeNamespace.footer]: footerStore,
    [appConfig.storeNamespace.layout]: layoutStore,
    [appConfig.storeNamespace.mailingLists]: mailingListsStore,
    [appConfig.storeNamespace.vacations]: vacationsStore,
    [appConfig.storeNamespace.search]: searchStore
  }
})
