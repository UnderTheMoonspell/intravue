import types from "@/mutation.types";
import appConfig from "@/app.config";

export default {
    [types.GET_SUCCESS](state, obj) {
        state.modules = obj.map(module => (
            {
                ...module,
                icon: appConfig.modulesIcons[module.caps]
            }
        ));
    },

    [types.ERROR](state, obj) {
        state.error = obj;
    }
};
