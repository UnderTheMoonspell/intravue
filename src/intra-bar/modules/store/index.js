import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import appConfig from "@/app.config";
import { createNamespacedHelpers } from "vuex";

const state = {
  modules: null,
  error: null
};

export const { mapGetters, mapActions } = createNamespacedHelpers(`${appConfig.storeNamespace.intrabar}/${appConfig.storeNamespace.modules}`);

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
