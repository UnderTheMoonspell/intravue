import DataService from "@/services/data.service";
import appConfig from "@/app.config";
import types from "@/mutation.types";

export default {
  async [types.GET]({ commit }) {
    try {
      let modules = await DataService.get(appConfig.APIURLs.getTopModules);
      commit(types.GET_SUCCESS, modules);
    } catch (ex) {
      console.warn(ex);
      commit(types.ERROR, "Erro no acesso ao servidor");
    }
  }
};
