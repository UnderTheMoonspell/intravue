import getters from './getters';
import mutations from './mutations';
import actions from './actions';
import modules from '../modules/store';
import appConfig from '@/app.config';
import { createNamespacedHelpers } from 'vuex';

const state = {
    drawer: null
}

export const { mapActions, mapGetters } = createNamespacedHelpers(`${appConfig.storeNamespace.intrabar}`);

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters,
    modules: {
        [appConfig.storeNamespace.modules]: modules
    }
}
