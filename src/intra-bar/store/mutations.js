import types from "@/mutation.types";

export default {
    [types.INTRABAR.SET_DRAWER](state, obj) {
        state.drawer = obj;
    }
}