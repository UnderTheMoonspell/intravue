import types from "@/mutation.types";
import appConfig from "@/app.config";
import DataService from "@/services/data.service";

export default {
    async [types.MOVE_SUBSCRIPTION]({ commit, dispatch }, { sub, position, subtype }) {
        try {
            await DataService.get(
                appConfig.APIURLs.moveSubscription(sub, position, subtype)
            );
            // switch (subtype) {
            //     case "subtype":
            //       dispatch(`${appConfig.storeNamespace.intrabar}/${appConfig.storeNamespace.projects}/GET}`);
            //       break;
            //     case "resources":
            //       dispatch(`${appConfig.storeNamespace.intrabar}/${appConfig.storeNamespace.modules}/GET}`);
            //       break;
            // }
        } catch (ex) {
            console.warn(ex);
        }
    },

    [types.INTRABAR.SET_DRAWER]({ commit }, drawer) {
        commit(types.INTRABAR.SET_DRAWER, drawer);
    }
};
