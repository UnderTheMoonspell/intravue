import getters from './getters';
import mutations from './mutations';
import actions from './actions';
import { createNamespacedHelpers } from 'vuex';
import appConfig from '@/app.config';

const state = {
    user: null,
    links: null,
    notifications: null,
    error: null,
    vacations: null,
    slackInfo: null,
    slackInviteStatus: null,
    isInvitingSlack: false,
    isLoadingSlack: false,
    userSkills: [],
    userDetail: null,
    imServices: null,
    socialNetworks: null,
    userSettings: null,
    boards: [],
    experimentalFeatures: [],
    departments: [],
    users: [],
    externalBossDomains: [],
    userProfiles: [],
    networkProfiles: [],
    statusTypes: appConfig.statusTypes,
    bossTypes: appConfig.bossTypes,
    allSkills: {},
    custeioCategories: [],
    custeioContracts: [],
    passwordStrength: 0,
    dashboardTabs: appConfig.userDashboardTabs,
    currentTab: 'summary'
}

const nameSpace = appConfig.storeNamespace.user;

export const { mapGetters, mapActions, mapMutations } = createNamespacedHelpers(nameSpace);

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
