import DataService from "@/services/data.service";
import appConfig from "@/app.config";
import types from "@/mutation.types";
import UtilService from '@/services/helper.service';
import i18nService, { labels } from '@/services/i18n.service';

export default {
    async [types.GET]({ commit }) {
        try {
            let user = await DataService.get(appConfig.APIURLs.getUser);
            commit(types.GET_SUCCESS, user);
            return user;
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_LINKS]({ commit, state }) {
        try {
            let links = await DataService.get(appConfig.APIURLs.getUserLinks);
            let order = await DataService.get(appConfig.APIURLs.getUserLinksOrder);

            commit(types.USER.GET_LINKS_SUCCESS, { links: orderedLinks(links, order) });
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_NOTIFICATIONS]({ commit }) {
        try {
            let notifications = await DataService.get(appConfig.APIURLs.getNotifications);
            commit(types.USER.GET_NOTIFICATIONS_SUCCESS, groupNotifications(notifications));
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.CLEAR_NOTIFICATIONS]({ commit, dispatch }, type) {
        try {
            await DataService.get(appConfig.APIURLs.clearNotifications(type));
            return dispatch(types.USER.GET_NOTIFICATIONS);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.DISMISS_NOTIFICATION]({ commit, dispatch }, id) {
        try {
            await DataService.get(appConfig.APIURLs.dismissNotification(id));
            dispatch(types.USER.GET_NOTIFICATIONS);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_DETAIL]({ commit }, username) {
        try {
            let userDetail = await DataService.get(appConfig.APIURLs.getUserDetail(username));
            commit(types.USER.GET_DETAIL_SUCCESS, userDetail);
            return userDetail;
        } catch (ex) {
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_VACATIONS]({ commit }, { user, start, end }) {
        try {
            let vacations = await DataService.get(appConfig.APIURLs.getUserVacations({ user, start, end }));
            commit(types.USER.GET_VACATIONS_SUCCESS, vacations);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_SLACK_INFO]({ commit, dispatch }, username) {
        try {
            commit(types.USER.LOADING_SLACK);
            let slackInfo = await DataService.get(appConfig.APIURLs.getSlackInfo(username));
            commit(types.USER.GET_SLACK_INFO_SUCCESS, slackInfo);
        } catch (ex) {
            // console.warn(ex);
            dispatch(types.USER.GET_SLACK_INVITE_STATUS, username);
        }
    },

    async [types.USER.GET_SLACK_INVITE_STATUS]({ commit }, username) {
        try {
            let invite = await DataService.get(appConfig.APIURLs.getSlackInviteStatus(username));
            commit(types.USER.GET_SLACK_INVITE_STATUS_SUCCESS, invite);
        } catch (ex) {
            // console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.INVITE_TO_SLACK]({ commit, dispatch }, username) {
        try {
            commit(types.USER.LOADING_INVITE_SLACK);
            await DataService.post(appConfig.APIURLs.inviteToSlack(username));
            dispatch(types.USER.GET_SLACK_INVITE_STATUS, username);
        } catch (ex) {
            // TODO TESTAR MENSAGENS DE ERRO AQUI
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_SKILLS]({ commit }, username) {
        try {
            let skills = await DataService.get(appConfig.APIURLs.getUserSkills(username));
            commit(types.USER.GET_SKILLS_SUCCESS, skills);
        } catch (ex) {
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.ENDORSE_SKILL]({ commit, dispatch }, object) {
        try {
            await DataService.post(appConfig.APIURLs.endorseSkill, object);
            dispatch(types.USER.GET_SKILLS, object.user);
        } catch (ex) {
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.REMOVE_ENDORSE_SKILL]({ commit, dispatch }, object) {
        try {
            await DataService.post(appConfig.APIURLs.removeEndorseSkill, object);
            dispatch(types.USER.GET_DETAIL, object.user);
        } catch (ex) {
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.EDIT]({ commit, dispatch }, { user, username }) {
        try {
            commit(`${appConfig.storeNamespace.layout}/${types.LAYOUT.CHANGE_OVERLAY_STATE}`, true, { root: true });
            await DataService.post(appConfig.APIURLs.editUser(username), UtilService.editUserForm(user));
            commit(`${appConfig.storeNamespace.layout}/${types.LAYOUT.CHANGE_OVERLAY_STATE}`, false, { root: true });
            // return dispatch(types.USER.GET_SKILLS, username);
        } catch (ex) {
            commit(types.ERROR, ex || "Erro no acesso ao servidor");
            throw ex || "Erro no acesso ao servidor";
        }
    },

    async [types.USER.GET_IM_SERVICES]({ commit }) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.getIMServices);
            commit(types.USER.GET_IM_SERVICES_SUCCESS, obj);
        } catch (ex) {
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_SOCIAL_NETWORKS]({ commit }) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.getSocialNetworks);
            commit(types.USER.GET_SOCIAL_NETWORKS_SUCCESS, obj);
        } catch (ex) {
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_DEPARTMENTS]({ commit }, managed) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.getDeparments(managed));
            commit(types.USER.GET_DEPARTMENTS_SUCCESS, obj);
        } catch (ex) {
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_ALL]({ commit }) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.getAllUsers());
            commit(types.USER.GET_ALL_SUCCESS, obj);
        } catch (ex) {
            console.warn(ex)
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_ACCOUNT_PREFERENCES]({ commit }) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.getUserSettings);
            commit(types.USER.GET_ACCOUNT_PREFERENCES_SUCCESS, obj);
            return obj;
        } catch (ex) {
            console.warn(ex)
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_EXPERIMENTAL_FEATURES]({ commit }) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.getExperimentalFeatures);
            commit(types.USER.GET_EXPERIMENTAL_FEATURES_SUCCESS, obj);
            return obj;
        } catch (ex) {
            console.warn(ex)
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.EDIT_ACCOUNT_PREFERENCES]({ commit, dispatch }, { payload, username }) {
        try {
            await DataService.post(appConfig.APIURLs.editUser(username), payload);
            dispatch(types.USER.GET_ACCOUNT_PREFERENCES, payload);
            dispatch(types.USER.GET_LINKS);
        } catch (ex) {
            commit(types.ERROR, ex || "Erro no acesso ao servidor");
            throw ex || "Erro no acesso ao servidor";
        }
    },

    async [types.USER.GET_BOARDS]({ commit }, params) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.getUserBoards(params));
            commit(types.USER.GET_BOARDS_SUCCESS, obj);
            return obj;
        } catch (ex) {
            console.warn(ex)
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_EXTERNAL_BOSS_DOMAINS]({ commit }) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.getAllowedExternalBossDomains);
            commit(types.USER.GET_EXTERNAL_BOSS_DOMAINS_SUCCESS, obj);
        } catch (ex) {
            console.warn(ex)
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_USER_PROFILES]({ commit }) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.getUserProfiles);
            commit(types.USER.GET_USER_PROFILES_SUCCESS, obj);
        } catch (ex) {
            console.warn(ex)
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.GET_NETWORK_PROFILES]({ commit }) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.getNetworkProfiles);
            commit(types.USER.GET_NETWORK_PROFILES_SUCCESS, obj);
        } catch (ex) {
            console.warn(ex)
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.USER.UPLOAD_PHOTO]({ commit }, { photo, username }) {
        try {
            let headers = {
                'Accept-Language': 'en-US,en;q=0.8',
                'Content-Type': 'multipart/form-data;'
            };
            return await DataService.post(appConfig.APIURLs.uploadPhoto(username), photo, headers);
        } catch (ex) {
            console.warn(ex)
            throw ex;
        }
    },

    async [types.USER.FORGET_SKILLS]({ commit, dispatch }, skills) {
        try {
            await DataService.post(appConfig.APIURLs.forgetSkill, skills);
        } catch (ex) {
            dispatch(types.USER.GET_SKILLS, skills.user);
            console.warn(ex);
            throw ex;
        }
    },

    async [types.USER.ADD_SKILL]({ commit }, data) {
        try {
            return await DataService.post(appConfig.APIURLs.addSkill, data);
        } catch (ex) {
            console.warn(ex);
            throw ex;
        }
    },

    async [types.SKILLS.GET_ALL]({ commit }) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.allSkills());
            commit(types.SKILLS.GET_ALL_SUCCESS, obj);
        } catch (ex) {
            console.warn(ex);
            throw ex;
        }
    },

    async [types.SKILLS.CREATE_SKILL]({ commit }, data) {
        try {
            return await DataService.post(appConfig.APIURLs.createSkill, data);
        } catch (ex) {
            console.warn(ex);
            throw ex;
        }
    },

    async [types.USER.GET_CUSTEIO_CATEGORIES]({ commit }) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.getCategories);
            commit(types.USER.GET_CUSTEIO_CATEGORIES_SUCCESS, obj);
        } catch (ex) {
            console.warn(ex);
            throw ex;
        }
    },

    async [types.USER.GET_CUSTEIO_CONTRACTS]({ commit }) {
        try {
            let obj = await DataService.get(appConfig.APIURLs.getContracts);
            commit(types.USER.GET_CUSTEIO_CONTRACTS_SUCCESS, obj);
        } catch (ex) {
            console.warn(ex);
            throw ex;
        }
    },

    async [types.USER.CHANGE_PASSWORD]({ commit }, data) {
        try {
            commit(`${appConfig.storeNamespace.layout}/${types.LAYOUT.CHANGE_OVERLAY_STATE}`, true, { root: true });
            await DataService.post(appConfig.APIURLs.changePassword, data);
            commit(`${appConfig.storeNamespace.layout}/${types.LAYOUT.CHANGE_OVERLAY_STATE}`, false, { root: true });
            // commit(types.USER.GET_CUSTEIO_CONTRACTS_SUCCESS, obj);
        } catch (ex) {
            commit(`${appConfig.storeNamespace.layout}/${types.LAYOUT.CHANGE_OVERLAY_STATE}`, false, { root: true });
            console.warn(ex);
            throw ex;
        }
    },

    ['PASSWORD_STRENGTH']({ commit }, data) {
        commit(`PASSWORD_STRENGTH`, data);
    }
};



const orderedLinks = (links, linksOrder) => {
    return Object.keys(links)
        .filter(key => links[key].length)
        .map(key => ({
            group: key,
            links: links[key],
            order: Object.keys(linksOrder).find(orderKey => linksOrder[orderKey] === key)
        }))
        .sort((a, b) => a.order - b.order)
}

const groupNotifications = notifications => {
    let transformedNotifications = [];
    let notificationsType = {};
    let possibleTypes = ['user', 'project', 'ticket', 'task'];
    let typesLabels = {
        'user': labels.PROFILE,
        'project': labels.PROJECTS,
        'ticket': labels.TICKETS,
        'task': labels.TASKS
    };
    let ticketLabels = {
        'created': labels.NEW,
        'assigned': labels.CHANGED,
        'assign_remove': labels.CHANGED,
        'status_changed': labels.CHANGED,
        'commented': labels.CHANGED,
        'changed': labels.CHANGED,
        'removed': labels.REMOVED
    };

    for (let i = 0; i < notifications.length; i++) {
        let notification = notifications[i]
        let type = notification.scope;
        let strings;
        let placeholders;
        let j;
        let message;

        notification.date = UtilService.timeSince(notification.time_since);

        if (type === 'ticket') {
            notification.label = ticketLabels[notification.action];
            notification.green = ticketLabels[notification.action] === labels.NEW;
            notification.orange = ticketLabels[notification.action] === labels.CHANGED;
            notification.red = ticketLabels[notification.action] === labels.REMOVED;
        }

        if (notification.content.message && typeof notification.content.message === 'string') {
            strings = i18nService.getIntraBarText(notification.content.message).split('$');
            placeholders = notification.content.placeholders;

            j = 2;
            message = [];
            for (let k = 0; k < strings.length; k++) {
                let s = strings[k];
                if (j % 2 === 0) {
                    message.push({
                        value: s
                    });
                } else {
                    let aux = {};

                    if (placeholders[s].link !== undefined) {
                        aux['link'] = placeholders[s].link.indexOf('://') > -1 ? placeholders[s].link.replace('.pt/#', '.pt/') : `/${placeholders[s].link.substring(1)}`;
                    }

                    if (placeholders[s].translate !== undefined) {
                        aux['value'] = i18nService.getIntraBarText(placeholders[s].translate);
                    } else {
                        aux['value'] = placeholders[s].value;
                    }

                    if (aux.link === undefined) {
                        aux['value'] = aux.value;
                    }

                    message.push(aux);
                }
                j++;
            }
            notification.content.message = message;
        }

        if (notificationsType[type]) {
            notificationsType[type].push(notification);
        } else {
            notificationsType[type] = [notification];
        }
    }

    for (let i = 0; i < possibleTypes.length; i++) {
        let type = possibleTypes[i];
        if (type in notificationsType) {
            type = {
                type: type,
                typeLabel: typesLabels[type],
                count: notificationsType[type].length,
                notifications: notificationsType[type]
            };
            transformedNotifications.push(type);
        }
    }
    return transformedNotifications;
};
