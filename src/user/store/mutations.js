import types from "@/mutation.types";

export default {
    [types.GET_SUCCESS](state, obj) {
        state.user = obj;
    },

    [types.USER.GET_LINKS_SUCCESS](state, obj) {
        state.links = obj.links;
    },

    [types.USER.GET_NOTIFICATIONS_SUCCESS](state, obj) {
        state.notifications = obj;
    },

    [types.USER.GET_DETAIL_SUCCESS](state, obj) {
        state.userDetail = obj;
    },

    [types.USER.GET_VACATIONS_SUCCESS](state, obj) {
        state.vacations = obj;
    },

    [types.USER.LOADING_SLACK](state) {
        state.isLoadingSlack = true;
    },

    [types.USER.LOADING_INVITE_SLACK](state) {
        state.isInvitingSlack = true;
    },

    [types.USER.GET_SLACK_INFO_SUCCESS](state, obj) {
        state.slackInfo = obj;
        state.isLoadingSlack = false;
    },

    [types.USER.GET_SLACK_INVITE_STATUS_SUCCESS](state, obj) {
        state.slackInfo = { ...state.slackInfo, ...obj };
        state.isLoadingSlack = false;
        state.isInvitingSlack = false;
    },

    [types.USER.GET_SKILLS_SUCCESS](state, obj) {
        state.userSkills = obj;
    },

    [types.ERROR](state, obj) {
        console.warn(obj)
        state.error = obj;
        state.isLoadingSlack = false;
        state.isInvitingSlack = false;
    },

    [types.CLEAR_STATE](state) {
        state.error = false;
        state.isLoadingSlack = false;
        state.isInvitingSlack = false;
        state.userDetail = {};
        state.slackInfo = null;
        state.vacations = null;
        state.userSkills = null;
        state.imServices = null;
        state.socialNetworks = null;
        state.departments = [];
        // state.users = [];
        state.externalBossDomains = [];
        state.userProfiles = [];
        state.networkProfiles = [];
        state.allSkills = [];
    },

    [types.USER.GET_IM_SERVICES_SUCCESS](state, obj) {
        state.imServices = obj;
    },

    [types.USER.GET_SOCIAL_NETWORKS_SUCCESS](state, obj) {
        state.socialNetworks = obj;
    },

    [types.USER.GET_DEPARTMENTS_SUCCESS](state, obj) {
        state.departments = obj;
    },

    [types.USER.GET_ALL_SUCCESS](state, obj) {
        state.users = obj;
    },

    [types.USER.GET_ACCOUNT_PREFERENCES_SUCCESS](state, obj) {
        state.userSettings = obj;
    },

    [types.USER.GET_EXPERIMENTAL_FEATURES_SUCCESS](state, obj) {
        state.experimentalFeatures = obj;
    },

    [types.USER.GET_BOARDS_SUCCESS](state, obj) {
        state.boards = obj;
    },

    [types.USER.GET_EXTERNAL_BOSS_DOMAINS_SUCCESS](state, obj) {
        state.externalBossDomains = obj;
    },

    [types.USER.GET_USER_PROFILES_SUCCESS](state, obj) {
        state.userProfiles = obj;
    },

    [types.USER.GET_NETWORK_PROFILES_SUCCESS](state, obj) {
        state.networkProfiles = obj;
    },

    // VER SE VALE A PENA CRIAR UMA STORE SO POS SKILLS
    [types.SKILLS.GET_ALL_SUCCESS](state, obj) {
        state.allSkills = obj;
    },

    [types.USER.GET_CUSTEIO_CONTRACTS_SUCCESS](state, obj) {
        state.custeioContracts = obj;
    },

    [types.USER.GET_CUSTEIO_CATEGORIES_SUCCESS](state, obj) {
        state.custeioCategories = obj;
    },

    PASSWORD_STRENGTH(state, obj) {
        state.passwordStrength = obj;
    },

    [types.USER.DASHBOARD.SET_TAB](state, obj) {
        state.currentTab = obj;
    }
};
