import moment from 'moment';
import { getField } from 'vuex-map-fields';
import types from '@/mutation.types';
import UtilService from '@/services/helper.service';
import { labels } from '@/services/i18n.service';

export default {
    user: state => state.user,
    links: state => state.links,
    notifications: state => state.notifications,
    notificationsCounter: state => state.notifications && state.notifications.reduce((sum, currNot) => sum + currNot.count, 0),
    error: state => state.error,
    toolLinks: state => state.links && state.links.find(links => links.group === 'tools'),
    managementLinks: state => state.links && state.links.find(links => links.group === 'management'),
    globalLinks: state => state.links && state.links.find(links => links.group === 'global'),
    usefulLinks: state => state.links && state.links.find(links => links.group === 'useful'),
    vacations: state => state.vacations,
    vacationsItems: state => state.vacations && ({
        month: state.vacations
            .filter((vacation) => moment(vacation.vacation_date).startOf('month').isSame(moment().startOf('month'))).length,
        nextMonth: state.vacations
            .filter((vacation) => moment(vacation.vacation_date).startOf('month').isSame(moment().startOf('month').add(1, 'months'))).length,
        year: state.vacations
            .filter((vacation) => moment(vacation.vacation_date).startOf('year').isSame(moment().startOf('year'))).length,
        unused: state.vacations
            .filter((vacation) => moment(vacation.vacation_date).isAfter(moment())).length
    }),
    slackInfo: state => state.slackInfo,
    isLoadingSlack: state => state.isLoadingSlack,
    isInvitingSlack: state => state.isInvitingSlack,
    endorsedSkills: state => {
        if (!state.userSkills || !state.userSkills.length) return [];
        let endorsers = [];
        return state.userSkills.map(skill => {
            skill.endorsers = [];
            skill.endorse = false;
            for (let j = 0; j < skill.endorsements.length; j++) {
                let endorser = skill.endorsements[j];
                if (!skill.endorsers.length || skill.endorsers.indexOf(endorser) > -1) {
                    if (endorser.unix_username === state.user) {
                        skill.endorsers = [endorser].concat(endorsers);
                    } else {
                        skill.endorsers.push(endorser);
                    }
                    skill.endorse = true;
                }
            }
            return skill;
        })
    },
    skills: state => state.userSkills,
    allSkills: state => state.allSkills.data,
    userDetail: state => state.userDetail,
    imServices: state => state.imServices,
    socialNetworks: state => state.socialNetworks,
    [types.USER.GET_FORM](state) {
        return getField(state.userDetail)
    },
    departments: state => state.departments && [{ label: 'N/A', value: null }].concat(UtilService.transformToCustomSelect(state.departments, 'pk', 'name')),
    externalBossDomains: state => state.externalBossDomains,
    userSettings: state => state.userSettings,
    experimentalFeatures: state => state.experimentalFeatures,
    allUsers: state => state.users,
    boards: state => state.boards,
    tasks: state => state.tasks,
    userProfiles: state => state.userProfiles,
    networkProfiles: state => state.networkProfiles,
    statusTypes: state => state.statusTypes,
    bossTypes: state => state.bossTypes,
    custeioCategories: state => state.custeioCategories && [{ label: 'N/A', value: null }].concat(state.custeioCategories),
    custeioContracts: state => state.custeioContracts && [{ label: 'N/A', value: null }].concat(state.custeioContracts),
    passwordStrength: state => {
        let strength = state.passwordStrength;
        if (strength) {
            if (strength < 40) {
                return {
                    class: 'weak',
                    label: labels.PASSSTRENGTHWEAK
                }
            } else if (strength >= 40 && strength < 60) {
                return {
                    class: 'medium',
                    label: labels.PASSSTRENGTHMEDIUM
                }
            } else if (strength >= 60 && strength <= 80) {
                return {
                    class: 'strong',
                    label: labels.PASSSTRENGTHSTRONG
                }
            } else if (strength > 80) {
                return {
                    class: 'verystrong',
                    label: labels.PASSSTRENGTHVERYSTRONG
                }
            }
        } else {
            return null;
        }
    },
    dashboardTabs: state => state.dashboardTabs,
    currentTab: state => state.currentTab
};

