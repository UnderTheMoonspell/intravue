import Vue from 'vue';
import Vuex from 'vuex';
import getters from '../../store/getters';
import mutations from '../../store/mutations';

Vue.use(Vuex);

const actions = {
    GET: jest.fn()
};

const state = {
    title: 'Store Title',
    subtitle: 'Store Sub Title',
    cartoons: [{ id: 1}, {id: 2}, {id:3}]
}

export function __createMocks() {
    return {
        actions,
        state,
        getters,
        mutations,
        store: new Vuex.Store({
            actions,
            state,
        }),
    };
}

export const store = {
    state,
    actions,
    getters,
    mutations,
    namespaced: true,
}