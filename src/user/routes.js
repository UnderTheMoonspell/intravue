import IntraBar from "@/intra-bar/IntraBar";
import CustomFooter from "@/footer/CustomFooter";
export default [
    {
        path: "/user/:username",
        // name: "user",
        components: {
            default: () => import(/* webpackChunkName: "user-dashboard" */ "./UserDashboard.vue"),
            "intra-bar": IntraBar,
            "footer": CustomFooter
        },
        // props: {
        //     default: route => ({
        //         componentPath: () =>
        //             import(/* webpackChunkName: "user-dashboard" */ "./UserDashboard.vue"),
        //         extras: {
        //             id: route.params.username
        //         }
        //     })
        // },
        children: [
            {
                // TODO METER O userId POR AQUI COM O route.params.username para depois fazer o isself
                path: "summary",
                name: "summary",
                component: () => import(/* webpackChunkName: "user-summary" */ "./summary/UserSummary.vue"),
                props: {
                    helpTOCId: 'user/summary'
                }
            },
            {
                path: "summary/edit",
                name: "summaryEdit",
                component: () => import(/* webpackChunkName: "user-summary-edit" */ "./summary/UserSummaryEdit.vue"),
                props: {
                    helpTOCId: 'user/summary'
                }
            },
            {
                path: "projects",
                name: "projects",
                component: () => import(/* webpackChunkName: "user-projects" */ "./projects/UserProjects.vue"),
                props: {
                    helpTOCId: 'user/projects'
                }
            },
            {
                path: "tickets",
                name: "tickets",
                component: () => import(/* webpackChunkName: "user-tickets" */ "./tickets/UserTickets.vue"),
                props: {
                    helpTOCId: 'user/tickets'
                }
            },
            {
                path: "tasks",
                name: "tasks",
                component: () => import(/* webpackChunkName: "user-tasks" */ "./tasks/UserTasks.vue"),
                props: {
                    helpTOCId: 'user/tasks'
                }
            },
            {
                path: "lists",
                name: "lists",
                component: () => import(/* webpackChunkName: "user-lists" */ "./mailing-lists/UserMailingLists.vue"),
                props: {
                    helpTOCId: 'user/lists'
                }
            },
            {
                path: "vacations",
                name: "vacations",
                component: () => import(/* webpackChunkName: "user-vacations" */ "./vacations/UserVacations.vue"),
                props: {
                    helpTOCId: 'user/vacations'
                }
            },
            {
                path: '',
                redirect: { name: "summary" }
            }
        ]
    }
];
