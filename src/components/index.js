import CustomButton from "./CustomButton.vue";
import CustomBarLink from "./CustomBarLink.vue";
import TopNav from "./TopNav.vue";
import CustomSelect from "./CustomSelect.vue";
import Loader from "./Loader.vue";
import LazyLoadScroll from "./LazyLoadScroll.vue";
import MailingListItem from "./MailingListItem.vue";
import OptionsFilter from "./OptionsFilter.vue";
import CustomImg from "./CustomImg";
import CustomPagination from "./CustomPagination";
import SmallModal from './SmallModal';
import EditBar from './EditBar.vue';
import LoadingOverlay from './LoadingOverlay.vue';
import ToastPanel from './ToastPanel.vue';
import FormInput from './FormInput.vue';
import draggable from "vuedraggable";
import AutoComplete from './AutoComplete.vue';
import AutoCompleteBoss from './AutoCompleteBoss.vue';
import CustomTextArea from './CustomTextArea.vue';
import CustomCheckbox from './CustomCheckbox.vue';
import CustomGroupCheckbox from './CustomGroupCheckbox.vue';
import CustomRadio from './CustomRadio.vue';
import AutoCompleteSkills from './AutoCompleteSkills';
import CustomModal from './CustomModal';
import ContextNav from './ContextNav';

export default {
    "custom-btn": CustomButton,
    "custom-bar-link": CustomBarLink,
    "top-nav": TopNav,
    "context-nav": ContextNav,
    "lazy-load-scroll": LazyLoadScroll,
    "loader": Loader,
    "mailing-list-item": MailingListItem,
    "options-filter": OptionsFilter,
    "custom-img": CustomImg,
    "custom-select": CustomSelect,
    "custom-text-area": CustomTextArea,
    "custom-checkbox": CustomCheckbox,
    "custom-group-checkbox": CustomGroupCheckbox,
    "custom-radio": CustomRadio,
    "custom-pagination": CustomPagination,
    "custom-modal": CustomModal,
    "small-modal": SmallModal,
    "edit-bar": EditBar,
    "loading-overlay": LoadingOverlay,
    "toast-panel": ToastPanel,
    "form-input": FormInput,
    "draggable": draggable,
    "auto-complete": AutoComplete,
    "auto-complete-boss": AutoCompleteBoss,
    "auto-complete-skills": AutoCompleteSkills

};
