import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import appConfig from "@/app.config";
import { createNamespacedHelpers } from "vuex";

const state = {
  mailingLists: [],
  events: [],
  eventsFilters: [],
  isLoading: false,
  error: null,
  userMailingLists: [],
  userMailingListsCount: 0
};

export const { mapGetters, mapActions } = createNamespacedHelpers(`${appConfig.storeNamespace.mailingLists}`);

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
