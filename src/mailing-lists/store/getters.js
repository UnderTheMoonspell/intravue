export default {
    mailingLists: state => state.mailingLists,
    events: state => state.events,
    eventsFilters: state => state.eventsFilters,
    error: state => state.error,
    userMailingLists: state => state.userMailingLists,
    userMailingListsCount: state => state.userMailingListsCount,
    isLoading: state => state.isLoading
}

