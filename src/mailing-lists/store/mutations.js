import types from "@/mutation.types";

export default {

    [types.MAILING_LISTS.GET_MAILING_LISTS_SUCCESS](state, obj) {
        state.mailingLists = obj.data;
        state.isLoading = false;
    },

    [types.MAILING_LISTS.GET_USER_MAILING_LISTS_SUCCESS](state, obj) {
        state.userMailingLists = obj.data;
        state.userMailingListsCount = obj.count;
        state.isLoading = false;
    },

    [types.LOADING](state) {
        state.isLoading = true;
    },

    [types.ERROR](state, obj) {
        state.error = obj;
    }
};
