import DataService from "@/services/data.service";
import appConfig from "@/app.config";
import types from "@/mutation.types";

export default {

    async [types.MAILING_LISTS.GET_MAILING_LISTS]({ commit }, { user, order, type, start, end }) {
        try {
            let userMailingLists = await DataService.get(appConfig.APIURLs.getUserMailingLists({ user, order, type, start, end }));
            commit(types.MAILING_LISTS.GET_MAILING_LISTS_SUCCESS, userMailingLists);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.MAILING_LISTS.GET_USER_MAILING_LISTS]({ commit }, { user, order, type, start, end }) {
        try {
            let userMailingLists = await DataService.get(appConfig.APIURLs.getUserMailingLists({ user, order, type, start, end }));
            commit(types.MAILING_LISTS.GET_USER_MAILING_LISTS_SUCCESS, userMailingLists);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    }
};
