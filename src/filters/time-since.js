import moment from 'moment';
import UtilService from '@/services/helper.service';

export default function(value) {
    if (value) {
        let result = moment().diff(moment(value), 'seconds');
        return UtilService.timeSince(result);
    }
}