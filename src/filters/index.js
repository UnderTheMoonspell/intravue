import TimeSince from './time-since';
import DefaultDate from './default-date';


export default {
    'timeSince': TimeSince,
    'date': DefaultDate
}