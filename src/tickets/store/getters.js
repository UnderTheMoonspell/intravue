export default {
    tickets: state => state.tickets.data,
    topTickets: state => state.topTickets,
    error: state => state.error
}

