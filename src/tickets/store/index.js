import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import appConfig from "@/app.config";
import { createNamespacedHelpers } from "vuex";

const state = {
  tickets: {
    data: [],
    count: 0
  },
  topTickets: [],
  error: null
};

export const { mapGetters, mapActions } = createNamespacedHelpers(`${appConfig.storeNamespace.tickets}`);

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
