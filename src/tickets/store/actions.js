import DataService from "@/services/data.service";
import appConfig from "@/app.config";
import types from "@/mutation.types";

export default {
    async [types.TICKETS.GET_TOP]({ commit }) {
        try {
            let tickets = await DataService.get(appConfig.APIURLs.getTopTickets);
            commit(types.TICKETS.GET_SUCCESS_TOP, tickets);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },
    async [types.GET]({ commit }, { user, filter }) {
      try {
        let tickets = await DataService.get(appConfig.APIURLs.getTickets(user, filter));
        commit(types.GET_SUCCESS, tickets);
        return tickets;
      } catch (ex) {
        console.warn(ex);
        commit(types.ERROR, "Erro no acesso ao servidor");
      }
    }
};
