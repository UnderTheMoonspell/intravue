import types from "@/mutation.types";

export default {
    [types.GET_SUCCESS](state, obj) {
        state.tickets = obj;
    },

    [types.TICKETS.GET_SUCCESS_TOP](state, obj) {
        state.topTickets = obj.map(ticket => {
            let green = ['new', 'reopened', 'accepted'].indexOf(ticket.status) > -1;
            let red = ticket.status === 'closes';
            let blue = ticket.status === 'assigner';
            return {
                ...ticket,
                green,
                blue,
                red,
                orange: !(green || ticket.blue || ticket.red)
            }
        });
    },

    [types.ERROR](state, obj) {
        state.error = obj;
    }
};
