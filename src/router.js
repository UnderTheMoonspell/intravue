import Vue from "vue";
import Router from "vue-router";
import Dashboard from "@/dashboard/Dashboard";
import Preferences from "@/preferences/Preferences";
import IntraBar from "@/intra-bar/IntraBar";
import MainError from "@/error-pages/MainError";
import SearchRoute from "@/search/routes";
import UserRoute from "@/user/routes";
import ProjectRoute from "@/projects/routes";
import CustomFooter from '@/footer/CustomFooter';
import { labels } from "@/services/i18n.service";

Vue.use(Router);

export default new Router({
    mode: "history",
    routes: [
        {
            path: "/dashboard",
            name: "dashboard",
            components: {
                default: Dashboard,
                "intra-bar": IntraBar,
                "footer": CustomFooter
            },
            props: {
                default: {
                    "nav-title": labels.DASHBOARD
                }
            }
        },
        {
            path: "/",
            redirect: { name: "dashboard" }
        },
        {
            path: "*",
            components: {
                default: MainError,
                "intra-bar": IntraBar,
                "footer": CustomFooter
            },
            props: {
                msg: "Página não encontrada."
            }
        },
        {
            path: "/invalid-credentials",
            name: "invalid-credentials",
            component: () => import(/* webpackChunkName: "invalid-credentials" */ "./error-pages/InvalidCredentials.vue")
        },
        {
            path: "/preferences/",
            redirect: "/preferences/general"
        },
        {
            path: "/preferences/:view",
            name: "preferences",
            components: {
                default: Preferences,
                "intra-bar": IntraBar,
                "footer": CustomFooter
            },
            props: {
                default: {
                    "nav-title": labels.PREFERENCES
                }
            }
        }
    ].concat(SearchRoute, UserRoute, ProjectRoute)
});