import { labels } from '@/services/i18n.service';
import { ptBR } from 'vuejs-datepicker/dist/locale'

export const baseURL = "http://localhost";

const baseAPIURL = `${baseURL}/api/v2`;

export default {
    APIURLs: {
        /** User */
        getUser: `${baseAPIURL}/user/whoami`,
        getUserLinks: `${baseAPIURL}/user/bar_links`,
        getAllUsers: (order = 'name', filter = 'all') => `${baseAPIURL}/user/list?order=${order}&filter=${filter}`,
        getUserLinksOrder: `${baseAPIURL}/user/bar_links_order`,
        uploadPhoto: (user) => `${baseAPIURL}/user/upload_photo/${user}`,
        getIMServices: `${baseAPIURL}/user/list_possible_im_services`,
        getSocialNetworks: `${baseAPIURL}/user/list_possible_networks`,
        getUserProfiles: `${baseAPIURL}/list_user_profiles`,
        getDeparments: (managed = false) => `${baseAPIURL}/department/list?managed=${managed}`,
        getHolidays: ({ since, until }) => `${baseAPIURL}/holiday/list?since=${since}&until=${until}`,
        getUserSettings: `${baseAPIURL}/user/account_preferences`,
        getUserBoards: ({ pk, order = 'name' }) => {
            return `${baseAPIURL}/user/list_boards/${pk}?order=${order}`;
        },
        getAllowedExternalBossDomains: `${baseAPIURL}/user/allowed_external_boss_domains`,
        getNetworkProfiles: `${baseAPIURL}/list_network_profiles`,
        getUserDetail: (user) => `${baseAPIURL}/user/detail/${user}`,
        editUser: (user) => `${baseAPIURL}/user/edit/${user}`,
        validatePassword: `${baseAPIURL}/user/valid_password`,
        changePassword: `${baseAPIURL}/user/change_password`,

        /** Projects */
        getTopProjects: `${baseAPIURL}/user/list_top_projects`,
        getProjects: ({ user, order = 'name', type = 'all', search = 'active', status, start = 0, end = 30 }) => {
            return `${baseAPIURL}/user/list_projects/${user}?order=${order}&type=${type}&search=${search}${status ? `&status=${status}` : ''}&start=${start}&end=${end}`;
        },
        getProjectsHistory: (user, start = 0, end = 30) => `${baseAPIURL}/user/list_projects_history/${user}?start=${start}&end=${end}`,
        subscribeProject: (unixUsername) => `${baseAPIURL}/project/subscribe_project/${unixUsername}`,
        unSubscribeProject: (unixUsername) => `${baseAPIURL}/project/unsubscribe_project/${unixUsername}`,
        getProjectsFilters: `${baseAPIURL}/user/list_projects_filters`,
        getProjectsEvents: (unixUsername, limit = 5, offset = 0) => `${baseAPIURL}/project/list_events/${unixUsername}?limit=${limit}&offset=${offset}`,
        getProjectDetail: (unixUsername) => `${baseAPIURL}/project/detail/${unixUsername}`,
        getProjectResources: ({ unixUsername, order = '-modified_on', type = 'all', status = 'active', start = 0, end = 30 }) =>
            `${baseAPIURL}/project/list_resources/${unixUsername}?order=${order}&type=${type}&start=${start}&end=${end}&status=${status}`,

        /** Tickets */
        getTopTickets: `${baseAPIURL}/user/list_top_tickets`,
        getTickets: (user, filter, start = 0, end = 5) => {
            return `${baseAPIURL}/user/list_tickets/${user}?start=${start}&end=${end}&order=-modified_on${
                filter ? `&filter=${filter}` : ""
            }`;
        },

        /** Tasks */
        getTopTasks: `${baseAPIURL}/user/list_top_tasks`,
        getTasks: (user, filter, limit = 5) => {
            return `${baseAPIURL}/user/list_tasks/${user}?limit=5&order=-modified_on${
                filter ? `&filter=${filter}` : ""
            }`;
        },
        getTasksFilters: (user) => `${baseAPIURL}/user/list_tasks_filters/${user}`,

        /** Modules */
        getTopModules: `${baseAPIURL}/user/list_top_modules`,

        /** Notifications */
        getNotifications: `${baseAPIURL}/user/list_notifications`,
        clearNotifications: type => `${baseAPIURL}/user/clear_notifications?scope=${type}`,
        dismissNotification: id => `${baseAPIURL}/user/dismiss_notification?id=${id}`,

        /** Slack */
        getSlackInfo: (user) => `${baseAPIURL}/slack/user_info/${user}`,
        getSlackInviteStatus: (user) => `${baseAPIURL}/slack/invite_status/${user}`,
        inviteToSlack: (user) => `${baseAPIURL}/slack/invite/${user}`,

        /** Dashboard */
        moveSubscription: (sub, position, subtype) => {
            return `${baseAPIURL}/user/move_subscription?sub=${sub}${
                position ? `&position=${position}` : ""
            }&sub_type=${subtype}`;
        },
        getNews: (page) => `${baseAPIURL}/list_news${page ? `?page=${page}` : ''}`,
        getNewsUrl: `${baseAPIURL}/news_archive_url`,
        getFooterHelp: (param, language = 'pt') => `${baseAPIURL}/context_help_pages?context=docs/${language}/${param}`,

        // TODO VER ESTA QUESTAO DA LINGUA
        /** Vacations */
        getUserVacations: ({ user, start, end, inactive = false }) => `${baseAPIURL}/user/vacations/${user}?since=${start}&until=${end}&includeInactive=${inactive}`,
        editUserVacations: user => `${baseAPIURL}/user/vacations/${user}`,

        /** Mailing List */
        getMailingLists: ({ user, order = 'name', type = 'all', search, start = 0, end = 30 }) => {
            return `${baseAPIURL}/mailinglist/${user}?order=${order}&type=${type}${search ? `&search=${search}` : ''}&start=${start}&end=${end}`;
        },
        getUserMailingLists: ({ user, order = 'name', type = 'all', search, start = 0, end = 30 }) => {
            return `${baseAPIURL}/user/list_mailinglists/${user}?order=${order}&type=${type}${search ? `&search=${search}` : ''}&start=${start}&end=${end}`;
        },

        /** Skills */
        endorseSkill: `${baseAPIURL}/user_skill/endorse`,
        removeEndorseSkill: `${baseAPIURL}/user_skill/remove_endorsement`,
        getProposedSkills: (entries = 16) => `${baseAPIURL}/user_skill/propose_users?count=${entries}`,
        getUserSkills: (user) => `${baseAPIURL}/user/list_skills/${user}`,
        addSkill: `${baseAPIURL}/skill/claim`,
        createSkill: `${baseAPIURL}/skill/create`,
        forgetSkill: `${baseAPIURL}/skill/forget`,
        allSkills: (order, search, start = 0, end = -1) => `${baseAPIURL}/skill/list?start=${start}&end=${end}${order ? `&order=${order}` : ''}${search ? `&search=${search}` : ''}`,

        /** Outros */
        getExperimentalFeatures: `${baseAPIURL}/experimental_features`,

        /** Search */
        getSearch: ({ type = 'all', query, since = 0, until = 30, filters }) => {
            return `${baseAPIURL}/search?type=${type}&start=${since}&end=${until}${query ? `&query=${query}` : ''}${filters || ''}`
        },

        /** Custeio */
        getCategories: `${baseAPIURL}/category/list`,
        getContracts: `${baseAPIURL}/contract/list`
    },
    storeNamespace: {
        user: "$user",
        projects: "$projects",
        intrabar: "$intrabar",
        modules: "$modules",
        tickets: "$tickets",
        tasks: "$tasks",
        notifications: "$notifications",
        footer: "$footer",
        form: "$form",
        layout: "$layout",
        mailingLists: "$mailingLists",
        vacations: "$vacations",
        search: "$search"
    },
    linksGroups: {
        custom_links: "Custom Links",
        global: "Equipas Transversais",
        management: "Gestão",
        tools: "Ferramentas",
        useful: "Útil"
    },
    ticketsFilters: [
        { label: labels.OPENTICKETS, value: "open" },
        { label: labels.ASSIGNED, value: "assigned" },
        { label: labels.CREATED, value: "created" }
    ],
    localStorageKeys: {
        tasksFilters: 'TASK_FILTER_KEY'
    },
    skillsMaxEntries: 16,
    skillsVisibleUsers: 2,
    modulesIcons: {
        'wiki,tickets': 'module fa fa-paw',
        'tickets,wiki': 'module fa fa-paw',
        'tasks': 'module fa fa-tasks',
        'tickets': 'module fa fa-ticket',
        'wiki': 'module fa fa-file-code-o',
        'docstorage': 'module fa fa-hdd-o',
        'repository': 'module fa fa-code-fork',
        'vpn': 'module fa fa-lock',
        'mailinglist': 'module fa fa-envelope'
    },
    userDashboardTabs: {
        summary: 'summary',
        tickets: 'tickets',
        tasks: 'tasks',
        projects: 'projects',
        lists: 'lists',
        vacations: 'vacations'
    },
    projectDashboardTabs: {
        summary: 'summary',
        tickets: 'tickets',
        tasks: 'tasks',
        lists: 'lists',
        vacations: 'vacations',
        metrics: 'metrics'
    },
    usernameDomains: [
        '@sapo.corppt.com',
        '@ptcom.corppt.com',
        '@ptportugal.corppt.com',
        '@tmn.corppt.com',
        '@pt-sgps.corppt.com',
        '@ptsi.corppt.com',
        '@ptpro.corppt.com',
        '@ptprime.corppt.com',
        '@ptin.corppt.com',
        '@ptc.corppt.com',
        '@pt-contact.corppt.com',
        '@telecom.pt'
    ],
    languages: [
        { label: 'Português', value: 'pt' },
        { label: 'English', value: 'en' }
    ],
    preferences: {
        tabs: ["GENERAL", "NOTIFICATIONS", "EXPERIMENTAL"]
    },
    calendar: {
        display_format: "D [de] MMMM YYYY",
        api_format: "YYYY-MM-DD",
        api_key: "vacation_date",
        max_expiry_value: 365,
        default_value: 90,
        colors: ['#416E14', '#196446', '#5A4678', '#AA3773', '#BE4B19', '#962319'],
        config: {
            defaultView: 'month',
            header: {
                left: 'title',
                center: '',
                right: 'prev,today,next'
            },
            buttonText: {
                today: 'Hoje'
            },
            locale: 'pt',
            height: 'auto'
        },
        events: {
            selectable: true,
            startEditable: false,
            durationEditable: true,
            droppable: false
        }
    },
    defaultUsersListSize: 10,
    statusTypes: [{
        label: 'Activo',
        value: 'Active'
    }, {
        label: 'Inactivo',
        value: 'Inactive'
    }],
    disableDatesUserStatus: {
        to: new Date()
    },
    datepickerDefaultConfig: {
        datepickerDefaultFormat: 'dd-MM-yyyy',
        language: ptBR

    },
    bossTypes: [
        { label: labels.EMAIL_BOSSTYPE, value: 'email' },
        { label: labels.INTRAUSER, value: 'user' }
    ],
    search: {
        tabs: [ {
            label: "ALL",
            key: "all",
            filters: {
                order: [
                    { label: "RELEVANCY", value: "-score" },
                    { label: "ALPHABETICAL", value: "name" }
                ]
            }
        }, {
            label: "PROJECTS",
            key: "project",
            filters: {
                order: [
                    { label: "RELEVANCY", value: "-score" },
                    { label: "ALPHABETICAL", value: "name" }
                ]
            }
        },
        {
            label: "RESOURCES",
            key: "resource",
            filters: {
                type: [
                    { label: "ALL", value: "all" },
                    { label: "REPOSITORY", value: "repository" },
                    { label: "SHAREDFOLDER", value: "docstorage" },
                    { label: "TASKS", value: "tasks" },
                    { label: "TICKETING", value: "tickets" },
                    { label: "VPNPROFILE", value: "vpn" },
                    { label: "WIKI", value: "wiki" }
                ]
            }
        },
        {
            label: "TICKETS",
            key: "ticket",
            filters: {
                status: [
                    { label: "ALL", value: "all" },
                    { label: "OPEN", value: "open" },
                    { label: "CLOSED", value: "closed" }
                ],
                relation: [
                    { label: "ALL", value: "all" },
                    { label: "CREATEDBY", value: "reporter" },
                    { label: "ASSIGNEDTO", value: "owner" },
                    { label: "FOLLOWING", value: "cc" }
                ]
            }
        },
        {
            label: "TASKS",
            key: "task",
            filters: {
                relation: [
                    { label: "ALL", value: "all" },
                    { label: "CREATEDBY", value: "created" },
                    { label: "ASSIGNEDTO", value: "assignees" }
                ]
            }
        },
        {
            label: "PERSONS",
            key: "user",
            filters: {
                order: [
                    { label: "RELEVANCY", value: "-score" },
                    { label: "ALPHABETICAL", value: "name" }
                ],
                type: [
                    { label: "ALL", value: "all" },
                    { label: "SAPOMEMBER", value: "SAPO" },
                    { label: "EXTERNAL", value: "Parceiros" }
                ]
            }
        },
        {
            label: "DOCUMENTS",
            key: "document",
            filters: {
                type: [
                    { label: "ALL", value: "all" },
                    { label: "HELP", value: "help" },
                    { label: "WIKIPAGE", value: "wiki" },
                    { label: "SHAREDFOLDER", value: "resource" }
                ]
            }
        } ]
    },
    modal: {
        large: {
            width: 900,
            height: 600
        },
        small: {
            width: 800,
            height: 500
        },
        mini: {
            width: 550,
            height: 300
        }
    },

    validationMessages: {
        custom: {
            email: {
                required: labels.VALIDEMAIL,
                invalid: labels.VALIDEMAIL
            },
            name: {
                required: labels.VALIDNAME
            },
            username: {
                required: labels.VALIDUNIX
            },
            floor: {
                max_value: 'Valor tem que ser inferior a 9',
                min_value: 'Valor tem que ser maior ou igual a 0'
            },
            seat: {
                max_value: 'Valor tem que ser inferior a 40',
                min_value: 'Valor tem que ser maior ou igual a 1'
            },
            external_boss: {
                invalid: labels.VALIDEMAIL
            },
            corporate_username: {
                validUnix: labels.VALIDCORP
            },
            new_password: {
                validPassword: labels.VALIDPASS,
                required: labels.INSERTNEW
            },
            confirm_password: {
                confirmed: labels.CONFIRMFAIL
            },
            password: {
                required: labels.INSERTCURRENT
            }
        },
        messages: {
            email: labels.VALIDEMAIL
        }
    }
};