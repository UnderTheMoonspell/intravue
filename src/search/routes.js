export default [
    {
        path: '/search',
        name: 'search',
        components: {
            default: () => import(/* webpackChunkName: "search" */ './Search.vue'),
            'intra-bar': () => import(/* webpackChunkName: "intra" */ '../intra-bar/IntraBar.vue')
        }
        // props: {
        //     default: {
        //         componentPath: () => import(/* webpackChunkName: "search" */ './Search.vue'),
        //         extras: {
        //             title: 'Search'
        //         }
        //     }
        // }
    }
]