import DataService from "@/services/data.service";
import appConfig from "@/app.config";
import types from "@/mutation.types";

export default {
    async [types.SEARCH.PERFORM]({ commit }, { type, query, since, until, filters }) {
        try {
            commit(types.LOADING);
            let search = await DataService.get(appConfig.APIURLs.getSearch({ type, query, since, until, filters }));
            commit(types.SEARCH.PERFORM_SUCCESS, search);
            return search;
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    }
};