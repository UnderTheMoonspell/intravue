import types from "@/mutation.types";

export default {
    [types.SEARCH.PERFORM_SUCCESS](state, result) {
        state.search = result;
        state.isLoading = false;
    },

    [types.LOADING](state) {
        state.isLoading = true;
    }
};
