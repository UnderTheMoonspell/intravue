import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import appConfig from "@/app.config";
import { createNamespacedHelpers } from "vuex";

const state = {
  search: [],
  isLoading: false
};

export const { mapGetters, mapActions } = createNamespacedHelpers(`${appConfig.storeNamespace.search}`);

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
