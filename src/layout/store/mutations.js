import types from "@/mutation.types";
import i18nService from "@/services/i18n.service";

export default {
    [types.LAYOUT.CHANGE_OVERLAY_STATE](state, obj) {
        state.activeOverlay = obj;
    },

    [types.LAYOUT.OPEN_TOAST_PANEL](state, obj) {
        state.toastPanelLevel = obj.level;
        state.toastPanelMessage = i18nService.getText(obj.message) || obj.message;
        state.toastTimeout = obj.timeout || state.toastTimeout;
    },

    [types.LAYOUT.CLOSE_TOAST_PANEL](state) {
        state.toastPanelLevel = null;
        state.toastPanelMessage = null;
        state.toastTimeout = 3000;
    }
};
