export default {
    activeOverlay: state => state.activeOverlay,
    toastPanelMessage: state => state.toastPanelMessage,
    toastPanelLevel: state => state.toastPanelLevel,
    toastPanelTimeout: state => state.toastTimeout
}

