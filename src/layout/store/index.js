import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import appConfig from "@/app.config";
import { createNamespacedHelpers } from "vuex";

const state = {
    activeOverlay: false,
    toastPanelMessage: null,
    toastPanelLevel: null,
    toastTimeout: 3000
};

export const { mapGetters, mapMutations } = createNamespacedHelpers(
    `${appConfig.storeNamespace.layout}`
);

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
};
