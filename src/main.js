import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "@/store";
import components from "@/components";
import directives from "@/directives";
import filters from "@/filters";
import VueCookie from "vue-cookie";
// import ValidationService from '@/services/validations.service';
import VeeValidate from "vee-validate";

Vue.config.productionTip = false;

Object.keys(components).forEach(key => {
    Vue.component(key, components[key]);
});

Object.keys(directives).forEach(key => {
    Vue.directive(key, directives[key]);
});

Object.keys(filters).forEach(key => {
    Vue.filter(key, filters[key]);
});

Vue.use(VueCookie);
Vue.use(VeeValidate);

// ValidationService.init();

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount("#app");