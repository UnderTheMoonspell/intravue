import getters from './getters';
import mutations from './mutations';
import actions from './actions';
import appConfig from '@/app.config';
import { createNamespacedHelpers } from 'vuex';

const state = {
    news: [],
    page: 0,
    news_archive_url: '',
    userSkills: []
}

export const { mapActions, mapGetters } = createNamespacedHelpers(`${appConfig.storeNamespace.dashboard}`);

export default {
    namespaced: true,
    state,
    mutations,
    actions,
    getters
}
