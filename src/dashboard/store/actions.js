import types from "@/mutation.types";
import appConfig from "@/app.config";
import DataService from "@/services/data.service";

export default {
    async [types.GET]({ commit, state }) {
        try {
            let page = state.page + 1;
            let news = await DataService.get(appConfig.APIURLs.getNews(page));
            commit(types.GET_SUCCESS, { news, page });
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.NEWS.GET_URL]({ commit, state }) {
      try {
          let url = await DataService.get(appConfig.APIURLs.getNewsUrl);
          commit(`${types.NEWS.GET_URL_SUCCESS}`, url);
      } catch (ex) {
          console.warn(ex);
          commit(types.ERROR, "Erro no acesso ao servidor");
      }
    },

    async [types.SKILLS.GET_PROPOSED_SKILLS]({ commit }) {
        try {
            let userSkills = await DataService.get(appConfig.APIURLs.getProposedSkills(appConfig.skillsMaxEntries));
            commit(`${types.SKILLS.GET_PROPOSED_SKILLS_SUCCESS}`, userSkills);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.SKILLS.ENDORSE_SKILL]({ commit }, { userSkill }) {
        try {
            return await DataService.post(appConfig.APIURLs.endorseSkill, { skill: userSkill.skills.identifier, user: userSkill.unix_username });
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    }
};
