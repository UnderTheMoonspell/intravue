export default {
    news: state => state.news,
    newsArchiveUrl: state => state.news_archive_url,
    userSkills: state => state.userSkills
};
