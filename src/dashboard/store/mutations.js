import types from "@/mutation.types";

export default {
    [types.GET_SUCCESS](state, obj) {
        state.page = obj.page;
        state.news = state.news.concat(obj.news);
    },

    [types.NEWS.GET_URL_SUCCESS](state, obj) {
        state.news_archive_url = obj;
    },

    [types.SKILLS.GET_PROPOSED_SKILLS_SUCCESS](state, obj) {
        state.userSkills = obj.reduce((arr, userSkill) => {
            let newUserskill = userSkill.skills.map(skill => {
                return { ...userSkill, skills: skill };
            });
            return arr.concat(newUserskill);
        }, []);
    },

    [types.ERROR](state, obj) {
        state.error = obj;
    }
};

// function getTestSkills() {
//     return [
//         {
//             name: "Henrique Soares Pedroso dos Santos Vaz",
//             photo: "media/users/photos/henrique-s-vaz.jpg",
//             skills: [
//                 { identifier: "mysql", name: "MySQL" },
//                 { identifier: "wordpress", name: "Wordpress" },
//                 { identifier: "oop", name: "OOP" },
//                 { identifier: "http", name: "HTTP" },
//                 { identifier: "english", name: "English" },
//                 { identifier: "bash", name: "bash" },
//                 { identifier: "portuguese", name: "Português" },
//                 { identifier: "ajax", name: "AJAX" },
//                 { identifier: "google_analytics", name: "Google Analytics" },
//                 { identifier: "opencv", name: "OpenCV" },
//                 { identifier: "html", name: "HTML" },
//                 { identifier: "javascript__es6_", name: "JavaScript (ES6)" },
//                 { identifier: "php", name: "PHP" },
//                 { identifier: "python", name: "Python" },
//                 { identifier: "html_5", name: "HTML 5" },
//                 { identifier: "ubuntu", name: "Ubuntu" },
//                 { identifier: "dns", name: "DNS" },
//                 { identifier: "git", name: "Git" },
//                 { identifier: "apache", name: "Apache" },
//                 { identifier: "css_3", name: "CSS 3" },
//                 { identifier: "haskell", name: "Haskell" },
//                 { identifier: "ssh", name: "SSH" },
//                 { identifier: "linux", name: "Linux" },
//                 { identifier: "ftp", name: "FTP" },
//                 { identifier: "windows", name: "Windows" },
//                 { identifier: "nagios", name: "Nagios" },
//                 { identifier: "java", name: "Java" },
//                 { identifier: "e-commerce", name: "E-commerce" },
//                 { identifier: "latex", name: "LaTeX" },
//                 { identifier: "sql", name: "SQL" },
//                 { identifier: "javascript", name: "JavaScript" },
//                 { identifier: "data_structures", name: "Data Structures" },
//                 { identifier: "css", name: "CSS" }
//             ],
//             unix_username: "henrique-s-vaz"
//         },
//         {
//             name: "Ana Godinho",
//             photo: "media/users/photos/anagod.jpg",
//             skills: [
//                 {
//                     identifier: "produ__o_de_conte_dos",
//                     name: "Produção de conteúdos"
//                 },
//                 { identifier: "comunica__o", name: "Comunicação" }
//             ],
//             unix_username: "anagod"
//         },
//         {
//             name: "Francisco C. S. Nascimento",
//             photo: "media/users/photos/franciscocnascimento.jpg",
//             skills: [
//                 { identifier: "html_5", name: "HTML 5" },
//                 { identifier: "user_interface", name: "User Interface" },
//                 { identifier: "css_3", name: "CSS 3" },
//                 { identifier: "user_experience", name: "User experience" },
//                 { identifier: "illustration", name: "Illustration" }
//             ],
//             unix_username: "franciscocnascimento"
//         },
//         {
//             name: "André Cristiano da Silva Pardal",
//             photo: "media/users/photos/andre.c.pardal.jpg",
//             skills: [
//                 { identifier: "javascript__es6_", name: "JavaScript (ES6)" },
//                 { identifier: "java", name: "Java" },
//                 { identifier: "node_js", name: "Node.js" }
//             ],
//             unix_username: "andre.c.pardal"
//         },
//         {
//             name: "Alice Barroso Barcellos",
//             photo: "media/users/photos/aliceb.jpg",
//             skills: [
//                 {
//                     identifier: "edi__o_e_produ__o_de_conte_dos_",
//                     name: "Edição e produção de conteúdos."
//                 }
//             ],
//             unix_username: "aliceb"
//         },
//         {
//             name: "Gaspar Manuel Rocha Brogueira",
//             photo: "media/users/photos/gasparmbrogueira.jpg",
//             skills: [
//                 { identifier: "latex", name: "LaTeX" },
//                 { identifier: "quality_assurance", name: "Quality Assurance" },
//                 { identifier: "big_data", name: "Big Data" },
//                 { identifier: "eclipse", name: "Eclipse" },
//                 { identifier: "mapreduce", name: "MapReduce" },
//                 { identifier: "mysql", name: "MySQL" },
//                 { identifier: "c", name: "C" },
//                 { identifier: "mongodb", name: "MongoDB" },
//                 { identifier: "java", name: "Java" },
//                 { identifier: "pycharm", name: "PyCharm" },
//                 { identifier: "python", name: "Python" },
//                 { identifier: "django", name: "Django" },
//                 { identifier: "html", name: "HTML" }
//             ],
//             unix_username: "gasparmbrogueira"
//         },
//         {
//             name: "Luís Carmona",
//             photo: "media/users/photos/lpcc.jpg",
//             skills: [
//                 { identifier: "english", name: "English" },
//                 { identifier: "smarty", name: "Smarty" },
//                 { identifier: "typography", name: "Typography" },
//                 { identifier: "ink", name: "Ink" },
//                 { identifier: "portuguese", name: "Português" },
//                 { identifier: "usability", name: "Usability" },
//                 { identifier: "layout", name: "Layout" },
//                 { identifier: "css", name: "CSS" },
//                 { identifier: "adobe_photoshop", name: "Adobe PhotoShop" },
//                 { identifier: "html", name: "HTML" }
//             ],
//             unix_username: "lpcc"
//         },
//         {
//             name: "Fátima Gualdino",
//             photo: "media/users/photos/fgualdino.jpg",
//             skills: [
//                 { identifier: "product_manager", name: "Product Manager" },
//                 {
//                     identifier: "gest_o_de_parcerias",
//                     name: "Gestão de Parcerias"
//                 },
//                 { identifier: "project_manager", name: "Project Manager" },
//                 { identifier: "problem_solver", name: "Problem Solver" }
//             ],
//             unix_username: "fgualdino"
//         },
//         {
//             name: "Fernando José Gaspar Alves",
//             photo: "media/users/photos/fernandoalves.jpg",
//             skills: [{ identifier: "e-commerce", name: "E-commerce" }],
//             unix_username: "fernandoalves"
//         },
//         {
//             name: "Susana Mendes",
//             photo: "media/users/photos/smendes.jpg",
//             skills: [
//                 { identifier: "adobe_illustrator", name: "Adobe Illustrator" },
//                 { identifier: "adobe_flash", name: "Adobe Flash" },
//                 { identifier: "adobe_dreamweaver", name: "Adobe Dreamweaver" },
//                 { identifier: "adobe_photoshop", name: "Adobe PhotoShop" },
//                 { identifier: "css", name: "CSS" },
//                 { identifier: "html_5", name: "HTML 5" }
//             ],
//             unix_username: "smendes"
//         },
//         {
//             name: "Madalena Carvalho",
//             photo: "media/users/photos/mcarvalho.jpg",
//             skills: [
//                 { identifier: "project_manager", name: "Project Manager" },
//                 { identifier: "product_manager", name: "Product Manager" }
//             ],
//             unix_username: "mcarvalho"
//         },
//         {
//             name: "Rodolfo Diogo",
//             photo: "media/users/photos/rodolfo.jpg",
//             skills: [
//                 { identifier: "adobe_dreamweaver", name: "Adobe Dreamweaver" },
//                 { identifier: "css", name: "CSS" },
//                 { identifier: "layout", name: "Layout" },
//                 { identifier: "html_5", name: "HTML 5" },
//                 { identifier: "typography", name: "Typography" },
//                 { identifier: "adobe_illustrator", name: "Adobe Illustrator" },
//                 { identifier: "html", name: "HTML" },
//                 { identifier: "adobe_photoshop", name: "Adobe PhotoShop" },
//                 { identifier: "adobe_flash", name: "Adobe Flash" },
//                 {
//                     identifier: "macromedia_freehand_mx",
//                     name: "Macromedia Freehand Mx"
//                 },
//                 { identifier: "sketching", name: "Sketching" }
//             ],
//             unix_username: "rodolfo"
//         },
//         {
//             name: "Gonçalo Fonseca Araújo",
//             photo: "media/users/photos/garaujo.jpg",
//             skills: [
//                 { identifier: "golang", name: "Golang" },
//                 { identifier: "project_manager", name: "Project Manager" },
//                 { identifier: "procurement", name: "Procurement" },
//                 { identifier: "contract_manager", name: "Contract Manager" },
//                 { identifier: "controller", name: "Controller" },
//                 { identifier: "process_manager", name: "Process Manager" },
//                 { identifier: "outsourcer_manager", name: "Outsourcer Manager" }
//             ],
//             unix_username: "garaujo"
//         },
//         {
//             name: "Pedro Miguel Fernandes Cachaldora",
//             photo: "media/users/photos/xbdia50.jpg",
//             skills: [
//                 { identifier: "cassandra", name: "Cassandra" },
//                 { identifier: "vagrant", name: "Vagrant" },
//                 { identifier: "nagios", name: "Nagios" },
//                 { identifier: "snmp", name: "SNMP" },
//                 { identifier: "ip", name: "IP" },
//                 { identifier: "apache", name: "Apache" },
//                 { identifier: "postgresql", name: "PostgreSQL" },
//                 { identifier: "dns", name: "DNS" },
//                 { identifier: "ldap", name: "LDAP" },
//                 { identifier: "mysql", name: "MySQL" },
//                 { identifier: "tcp", name: "TCP" },
//                 { identifier: "apache_tomcat", name: "Apache Tomcat" },
//                 { identifier: "git", name: "Git" },
//                 { identifier: "c__", name: "C++" },
//                 { identifier: "iptables", name: "iptables" },
//                 { identifier: "ssh", name: "SSH" },
//                 { identifier: "dhcp", name: "DHCP" },
//                 { identifier: "sinatra", name: "Sinatra" },
//                 { identifier: "ink", name: "Ink" },
//                 { identifier: "java", name: "Java" },
//                 { identifier: "elasticsearch", name: "ElasticSearch" },
//                 { identifier: "html_5", name: "HTML 5" },
//                 { identifier: "vim", name: "Vim" },
//                 { identifier: "c", name: "C" },
//                 { identifier: "varnish", name: "Varnish" },
//                 { identifier: "cacti", name: "Cacti" },
//                 { identifier: "nginx", name: "Nginx" },
//                 {
//                     identifier: "regular_expressions",
//                     name: "Regular Expressions"
//                 },
//                 { identifier: "c__net", name: "C#.Net" },
//                 { identifier: "bash", name: "bash" },
//                 { identifier: "debian", name: "Debian" },
//                 { identifier: "linux", name: "Linux" },
//                 { identifier: "ruby", name: "Ruby" }
//             ],
//             unix_username: "xbdia50"
//         },
//         {
//             name: "Elias Miguel da Costa Barreira",
//             photo: "media/users/photos/xbdia52.jpg",
//             skills: [
//                 { identifier: "elasticsearch", name: "ElasticSearch" },
//                 { identifier: "ssh", name: "SSH" },
//                 { identifier: "quality_assurance", name: "Quality Assurance" },
//                 { identifier: "mysql", name: "MySQL" },
//                 { identifier: "ubuntu", name: "Ubuntu" },
//                 { identifier: "japanese", name: "Japanese" },
//                 { identifier: "mongodb", name: "MongoDB" },
//                 { identifier: "problem_solver", name: "Problem Solver" },
//                 { identifier: "ansible", name: "Ansible" },
//                 { identifier: "http", name: "HTTP" },
//                 { identifier: "debian", name: "Debian" },
//                 { identifier: "haskell", name: "Haskell" },
//                 { identifier: "iptables", name: "iptables" },
//                 { identifier: "pl-sql", name: "PL-SQL" },
//                 { identifier: "html_5", name: "HTML 5" },
//                 { identifier: "ftp", name: "FTP" },
//                 { identifier: "eclipse", name: "Eclipse" },
//                 { identifier: "linux", name: "Linux" },
//                 { identifier: "physics", name: "Physics" },
//                 { identifier: "sql", name: "SQL" },
//                 { identifier: "illustration", name: "Illustration" },
//                 { identifier: "portuguese", name: "Português" },
//                 { identifier: "data_structures", name: "Data Structures" },
//                 { identifier: "apache", name: "Apache" },
//                 { identifier: "javascript__es6_", name: "JavaScript (ES6)" },
//                 { identifier: "git", name: "Git" },
//                 { identifier: "latex", name: "LaTeX" },
//                 { identifier: "puppet", name: "Puppet" },
//                 { identifier: "grunt__js_", name: "Grunt (JS)" },
//                 { identifier: "meteor", name: "Meteor" },
//                 { identifier: "html", name: "HTML" },
//                 { identifier: "dns", name: "DNS" },
//                 { identifier: "chemistry", name: "Chemistry" },
//                 { identifier: "sqlite", name: "SQLite" },
//                 { identifier: "css", name: "CSS" },
//                 { identifier: "gluster_fs", name: "Gluster FS" },
//                 { identifier: "gimp", name: "GIMP" },
//                 { identifier: "ssl_tls", name: "SSL/TLS" },
//                 { identifier: "bash", name: "bash" },
//                 { identifier: "node_js", name: "Node.js" },
//                 { identifier: "english", name: "English" },
//                 { identifier: "c", name: "C" },
//                 { identifier: "prolog", name: "Prolog" },
//                 { identifier: "css_3", name: "CSS 3" },
//                 { identifier: "adobe_photoshop", name: "Adobe PhotoShop" },
//                 { identifier: "windows", name: "Windows" },
//                 { identifier: "mathematics", name: "Mathematics" },
//                 { identifier: "perl", name: "Perl" },
//                 { identifier: "javascript", name: "JavaScript" },
//                 {
//                     identifier: "edi__o_e_produ__o_de_conte_dos_",
//                     name: "Edição e produção de conteúdos."
//                 },
//                 { identifier: "django", name: "Django" },
//                 { identifier: "adobe_illustrator", name: "Adobe Illustrator" },
//                 { identifier: "dhcp", name: "DHCP" },
//                 { identifier: "nginx", name: "Nginx" },
//                 { identifier: "python", name: "Python" },
//                 { identifier: "encodings", name: "Encodings" },
//                 { identifier: "wordpress", name: "Wordpress" },
//                 {
//                     identifier: "regular_expressions",
//                     name: "Regular Expressions"
//                 },
//                 { identifier: "vagrant", name: "Vagrant" },
//                 { identifier: "magia", name: "Magia" },
//                 { identifier: "php", name: "PHP" },
//                 { identifier: "java", name: "Java" },
//                 { identifier: "french", name: "French" },
//                 {
//                     identifier: "produ__o_de_conte_dos",
//                     name: "Produção de conteúdos"
//                 },
//                 { identifier: "sql_server", name: "SQL Server" },
//                 { identifier: "web_security", name: "Web Security" },
//                 { identifier: "makefile", name: "Makefile" },
//                 { identifier: "oracle", name: "Oracle" },
//                 { identifier: "fotografia", name: "Fotografia" },
//                 { identifier: "ipv6", name: "IPV6" },
//                 { identifier: "c__net", name: "C#.Net" }
//             ],
//             unix_username: "xbdia52"
//         },
//         {
//             name: "Hugo Costa",
//             photo: "media/users/photos/hugocosta.jpg",
//             skills: [
//                 { identifier: "libsapo_js", name: "LibSAPO.js" },
//                 { identifier: "css", name: "CSS" },
//                 { identifier: "javascript", name: "JavaScript" },
//                 { identifier: "html", name: "HTML" },
//                 { identifier: "rest", name: "REST" },
//                 { identifier: "subversion", name: "Subversion" },
//                 { identifier: "vim", name: "Vim" },
//                 { identifier: "perl", name: "Perl" },
//                 { identifier: "apache", name: "Apache" },
//                 { identifier: "mysql", name: "MySQL" },
//                 { identifier: "nginx", name: "Nginx" },
//                 { identifier: "ajax", name: "AJAX" },
//                 { identifier: "sql", name: "SQL" },
//                 { identifier: "xml", name: "XML" },
//                 {
//                     identifier: "regular_expressions",
//                     name: "Regular Expressions"
//                 },
//                 { identifier: "rss", name: "RSS" }
//             ],
//             unix_username: "hugocosta"
//         }
//     ];
// }
