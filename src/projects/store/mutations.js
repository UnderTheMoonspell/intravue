import types from "@/mutation.types";

export default {
    [types.GET_SUCCESS](state, obj) {
        state.projects = obj.map(project => ({
            ...project,
            hasTasks: project.caps.indexOf('tasks') >= 0,
            hasTickets: project.caps.indexOf('tickets') >= 0,
            hasFiles: project.caps.indexOf('docstorage') >= 0
        }));
        state.isLoading = false;
    },

    [types.PROJECTS.GET_USER_PROJECTS_SUCCESS](state, obj) {
        state.userProjects = obj.data;
        state.userProjectsCount = obj.count;
        state.isLoading = false;
    },

    [types.PROJECTS.GET_USER_PROJECTS_HISTORY_SUCCESS](state, obj) {
        state.userProjectsHistory = obj.data;
        state.userProjectsHistoryCount = obj.count;
        state.isLoading = false;
    },

    [types.PROJECTS.GET_EVENTS_FILTERS_SUCCESS](state, obj) {
        state.eventsFilters = obj;
    },

    [types.PROJECTS.GET_EVENTS_SUCCESS](state, obj) {
        state.events = obj;
    },

    [types.PROJECTS.GET_DETAIL_SUCCESS](state, obj) {
        state.projectDetail = obj;
    },

    [types.PROJECTS.GET_RESOURCES_SUCCESS](state, obj) {
        state.projectResources = obj;
    },

    [types.CLEAR_STATE](state) {
        state.projectDetail = null;
        state.projectResources = null;
    },

    [types.LOADING](state) {
        state.isLoading = true;
    },

    [types.ERROR](state, obj) {
        state.error = obj;
    }
};
