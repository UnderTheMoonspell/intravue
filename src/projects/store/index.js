import getters from "./getters";
import mutations from "./mutations";
import actions from "./actions";
import appConfig from "@/app.config";
import { createNamespacedHelpers } from "vuex";

const state = {
  projects: null,
  events: [],
  eventsFilters: [],
  isLoading: false,
  error: null,
  userProjects: [],
  userProjectsCount: 0,
  userProjectsHistory: [],
  userProjectsHistoryCount: 0,
  projectDetail: null,
  projectResources: null,
  dashboardTabs: appConfig.projectDashboardTabs
};

export const { mapGetters, mapActions, mapMutations } = createNamespacedHelpers(`${appConfig.storeNamespace.projects}`);

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters
};
