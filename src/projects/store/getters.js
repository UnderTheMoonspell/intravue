import i18nService from '@/services/i18n.service';
import UtilService from '@/services/helper.service';
import projectConfig from '@/projects/projects.config';

export default {
    projects: state => state.projects,
    events: state => state.events,
    eventsFilters: state => state.eventsFilters,
    error: state => state.error,
    userProjects: state => state.userProjects,
    userProjectsCount: state => state.userProjectsCount,
    isLoading: state => state.isLoading,
    userProjectsHistory: state => state.userProjectsHistory,
    userProjectsHistoryCount: state => state.userProjectsHistoryCount,
    projectDetail: state => state.projectDetail,
    projectResources: state => {
        if (!state.projectResources) return null;
        let data = state.projectResources.data;
        let resources = [];
        let message;

        for (let i = 0; i < projectConfig.resourceLimit && i < data.length; i++) {
            let resource = { ...data[i] };
            resource.icon = projectConfig.resourcesIconMap[resource.enabled_capabilities];
            message = i18nService.getText(resource.info.message);
            if (resource.info.time) {
                message = i18nService.interpolate(message, [UtilService.timeSince(resource.info.time_since)]);
            }
            resource.info = message;
            resource.enabled_capabilities = i18nService.getText(resource.enabled_capabilities);
            resources.push(resource);
        }

        return resources;
        // let currentResources = self.resources();
        // resources.forEach(function (newResource) {
        //     var alreadyListed = currentResources.filter(function (resource) { return resource.pk === newResource.pk; }).length > 0;
        //     if (!alreadyListed) {
        //         currentResources.push(newResource);
        //     }
        // });
        // self.resources(currentResources);
        // self.moreResources(response.responseJSON.count > self.resources().length);
    },
    dashboardTabs: state => state.dashboardTabs
}

