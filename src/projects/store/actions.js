import DataService from "@/services/data.service";
import { IntraEvent } from "@/services/helper.service";
import appConfig from "@/app.config";
import types from "@/mutation.types";

export default {
    async [types.GET]({ commit }) {
        try {
            commit(types.LOADING);
            let projects = await DataService.get(appConfig.APIURLs.getTopProjects);
            commit(types.GET_SUCCESS, projects);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.PROJECTS.GET_USER_PROJECTS]({ commit }, { user, order, type, status, start, end }) {
        try {
            let userProjects = await DataService.get(appConfig.APIURLs.getProjects({ user, order, type, status, start, end }));
            commit(types.PROJECTS.GET_USER_PROJECTS_SUCCESS, userProjects);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.PROJECTS.GET_USER_PROJECTS_HISTORY]({ commit }, { user }) {
        try {
            let userProjects = await DataService.get(appConfig.APIURLs.getProjectsHistory(user));
            commit(types.PROJECTS.GET_USER_PROJECTS_HISTORY_SUCCESS, userProjects);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.PROJECTS.SUBSCRIBE_PROJECT]({ commit }, { unixName }) {
        try {
            return await DataService.post(appConfig.APIURLs.subscribeProject(unixName));
            // commit(types.PROJECTS.GET_USER_PROJECTS_HISTORY_SUCCESS, userProjects.data);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.PROJECTS.UNSUBSCRIBE_PROJECT]({ commit }, { unixName }) {
        try {
            return await DataService.post(appConfig.APIURLs.unSubscribeProject(unixName));
            // commit(types.PROJECTS.GET_USER_PROJECTS_HISTORY_SUCCESS, userProjects.data);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.PROJECTS.GET_EVENTS_FILTERS]({ commit }) {
        try {
            let filters = await DataService.get(appConfig.APIURLs.getProjectsFilters);
            commit(types.PROJECTS.GET_EVENTS_FILTERS_SUCCESS, filters);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.PROJECTS.GET_EVENTS]({ commit }, { project }) {
        try {
            let events = await DataService.get(appConfig.APIURLs.getProjectsEvents(project));
            events = events.map(event => new IntraEvent(event).transformEventMessage());
            commit(types.PROJECTS.GET_EVENTS_SUCCESS, events);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.PROJECTS.GET_DETAIL]({ commit }, projectId) {
        try {
            let events = await DataService.get(appConfig.APIURLs.getProjectDetail(projectId));
            commit(types.PROJECTS.GET_DETAIL_SUCCESS, events);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    },

    async [types.PROJECTS.GET_RESOURCES]({ commit }, { projectId, start, end }) {
        try {
            let resources = await DataService.get(appConfig.APIURLs.getProjectResources({ unixUsername: projectId, end, start }));
            commit(types.PROJECTS.GET_RESOURCES_SUCCESS, resources);
        } catch (ex) {
            console.warn(ex);
            commit(types.ERROR, "Erro no acesso ao servidor");
        }
    }
};
