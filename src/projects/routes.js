import IntraBar from "@/intra-bar/IntraBar";
import CustomFooter from "@/footer/CustomFooter";
export default [
    {
        path: "/project/:projectname",
        // name: "user",
        components: {
            default: () => import(/* webpackChunkName: "project-dashboard" */ "./ProjectDashboard.vue"),
            "intra-bar": IntraBar,
            "footer": CustomFooter
        },
        children: [
            {
                // TODO METER O userId POR AQUI COM O route.params.username para depois fazer o isself
                path: "summary",
                name: "projectSummary",
                component: () => import(/* webpackChunkName: "project-summary" */ "./summary/ProjectSummary.vue"),
                props: {
                    helpTOCId: 'projects/summary'
                }
            },
            {
                path: "tickets",
                name: "projectTickets",
                component: () => import(/* webpackChunkName: "project-tickets" */ "./tickets/ProjectTickets.vue"),
                props: {
                    helpTOCId: 'user/tickets'
                }
            },
            // {
            //     path: "tasks",
            //     name: "tasks",
            //     component: () => import(/* webpackChunkName: "user-tasks" */ "./tasks/UserTasks.vue"),
            //     props: {
            //         helpTOCId: 'user/tasks'
            //     }
            // },
            // {
            //     path: "lists",
            //     name: "lists",
            //     component: () => import(/* webpackChunkName: "user-lists" */ "./mailing-lists/UserMailingLists.vue"),
            //     props: {
            //         helpTOCId: 'user/lists'
            //     }
            // },
            // {
            //     path: "vacations",
            //     name: "vacations",
            //     component: () => import(/* webpackChunkName: "user-vacations" */ "./vacations/UserVacations.vue"),
            //     props: {
            //         helpTOCId: 'user/vacations'
            //     }
            // },
            {
                path: '',
                redirect: { name: "projectSummary" }
            }
        ]
    }
];
