export default {
    resourcesIconMap: {
        'wiki,tickets': 'fa fa-paw',
        'tickets,wiki': 'fa fa-paw',
        'tasks': 'fa fa-tasks',
        'tickets': 'fa fa-ticket',
        'wiki': 'fa fa-file-code-o',
        'docstorage': 'fa fa-hdd-o',
        'repository': 'fa fa-code-fork',
        'vpn': 'fa fa-lock',
        'mailinglist': 'fa fa-envelope',
        'slack': 'fa fa-slack'
    },

    resourceOffset: 6,
    resourceLimit: 6
};