import { labels } from '@/services/i18n.service';
import DataService from '@/services/data.service';
import UtilService from '@/services/helper.service';
import appConfig from "@/app.config";

export const VALIDATION_MESSAGES = {
    custom: {
        email: {
            required: labels.VALIDEMAIL,
            invalid: labels.VALIDEMAIL
        },
        name: {
            required: labels.VALIDNAME
        },
        username: {
            required: labels.VALIDUNIX
        },
        floor: {
            // TODO FAZER ESTAS LABELS
            max_value: 'Valor tem que ser inferior a 9',
            min_value: 'Valor tem que ser maior ou igual a 0'
        },
        seat: {
            // TODO FAZER ESTAS LABELS
            max_value: 'Valor tem que ser inferior a 40',
            min_value: 'Valor tem que ser maior ou igual a 1'
        },
        external_boss: {
            invalid: labels.VALIDEMAIL
        },
        corporate_username: {
            validUnix: labels.VALIDCORP
        },
        new_password: {
            validPassword: labels.VALIDPASS,
            required: labels.INSERTNEW
        },
        confirm_password: {
            confirmed: labels.CONFIRMFAIL
        },
        password: {
            required: labels.INSERTCURRENT
        }
    },
    messages: {
        email: labels.VALIDEMAIL
    }
}

const validatePassword = (value) => DataService.post(appConfig.APIURLs.validatePassword, { password: value });

export default {
    validUnix: {
        validate(value) {
            return !!value.match(/^[a-z0-9][0-9a-z_\-.]{2,}$/);
        }
    },

    validPassword: (store) => ({
        validate(value) {
            return UtilService.debounce(() => validatePassword(value), 500)
                .then(data => {
                    store.dispatch('$user/PASSWORD_STRENGTH', data.strength);
                    return { valid: data.valid };
                })
        }
    }),

    validDomains: validator => (value, domains) => validator.rules.email.validate(value) &&
        domains.filter(domain => value.indexOf(domain) > -1).length
}
