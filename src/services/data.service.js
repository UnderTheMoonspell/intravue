import axios from "axios";
import Vue from "vue";
import router from "@/router";

const defaultHeaders = {
    Accept: "application/json",
    "Content-Type": "application/json"
};

const axiosInstance = axios.create({
    withCredentials: true,
    timeout: 10000
});

axiosInstance.interceptors.request.use(response => {
    if (response.method.toUpperCase() !== "GET") {
        response.headers["X-CSRFToken"] = Vue.cookie.get("csrftoken");
    }
    return response;
});

axiosInstance.interceptors.response.use(
    response => response,
    err => {
        if (err.response.status === 401) {
            router.push("invalid-credentials");
        }
        throw err;
    }
);

export default {
    async get(url) {
        return axiosInstance.get(url).then(response => response.data);
    },

    async post(url, object, headers = {}) {
        try {
            const response = await axiosInstance.post(url, object, { headers: { ...defaultHeaders, ...headers } });
            return response.data;
        } catch (err) {
            throw err.response.data;
        }
    },

    getFromLocal(key) {
        return localStorage.getItem(key);
    },

    setLocal(key, obj) {
        localStorage.setItem(key, obj);
    }
};
