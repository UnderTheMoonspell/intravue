const _generateChannelTeamUrl = (channelId, teamId) => {
    var url = 'https://slack.com/app_redirect?channel={channel}';
    if (teamId) {
        url += '&team={team}';
    }

    return url
        .replace(/\{channel\}/, channelId)
        .replace(/\{team\}/, teamId);
};

const service = {
    generateUserUrl: (userId, teamId) => _generateChannelTeamUrl(userId, teamId)
};

export default service;
