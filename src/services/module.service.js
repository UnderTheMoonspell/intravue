import Vue from 'vue';
import store from '../store'

export default function registerModule(storeKey, newModule) {
    if (!(storeKey in store._modules.root._children)) {
        store.registerModule(storeKey, newModule.store);
    }

    if (newModule.filters && Object.keys(newModule.filters).length) {
        Object.keys(newModule.filters).forEach(key => {
            Vue.filter(key, newModule.filters[key])
        })
    }
}