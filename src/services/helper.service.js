import i18nService, { labels } from "./i18n.service";

import appConfig from '@/app.config';

import * as moment from "moment";

let timeoutID;

const service = {
    timeSince(seconds) {
        var output = labels.AGO;

        var interval = Math.floor(seconds / 31536000);

        if (interval > 1) {
            return i18nService.interpolate(
                output,
                { t: interval + labels.YEARS },
                true
            );
        }
        interval = Math.floor(seconds / 2592000);
        if (interval > 1) {
            return i18nService.interpolate(
                output,
                { t: interval + labels.MONTHS },
                true
            );
        }
        interval = Math.floor(seconds / 86400);
        if (interval > 1) {
            return i18nService.interpolate(
                output,
                { t: interval + labels.DAYS },
                true
            );
        }
        interval = Math.floor(seconds / 3600);
        if (interval > 1) {
            return i18nService.interpolate(
                output,
                { t: interval + labels.HOURS },
                true
            );
        }
        interval = Math.floor(seconds / 60);
        if (interval > 1) {
            return i18nService.interpolate(
                output,
                { t: interval + labels.MINUTES },
                true
            );
        }
        return i18nService.interpolate(
            output,
            { t: Math.floor(seconds) + labels.SECONDS },
            true
        );
    },

    getLink(link) {
        return `/${link.substring(1)}`;
    },

    async debounce(fn, delay) {
        clearTimeout(timeoutID);
        timeoutID = null;
        var args = arguments;
        var that = this;
        return new Promise(function(resolve) {
            timeoutID = setTimeout(function() {
                resolve(fn.apply(that, args));
            }, delay);
        });
    },

    shuffleArray(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    },

    formatAPIDate(date) {
        return moment(date).format(appConfig.calendar.api_format);
    },

    transformToCustomSelect(array, valueProp, labelProp) {
        return array.map((arrObj, idx) => ({
            value: valueProp ? arrObj[valueProp] : idx,
            label: labelProp ? arrObj[labelProp] : arrObj
        }));
    },

    copyForm(userDetail) {
        // function getPhoneNumbers() {
        //     return userDetail.phone_contacts.reduce((arr, phone) => arr.concat(`${phone.prefix} ${phone.number},'`), '')
        // }

        return {
            boss_type: userDetail.boss_type || null,
            boss: userDetail.boss_unix_username || null,
            department: userDetail.department_id || null,
            description: userDetail.description || null,
            email: userDetail.email || null,
            employer: userDetail.employer || null,
            external_boss: userDetail.external_boss || null,
            floor: userDetail.floor || null,
            im_services: userDetail.im_services.slice() || [],
            name: userDetail.name || null,
            phone_numbers: userDetail.phone_contacts.slice() || null,
            seat: userDetail.seat || null,
            social_networks: userDetail.social_networks.slice() || [],
            ssh_key: userDetail.ssh_key || null,
            title: userDetail.title || null,
            corporate_username: userDetail.corporate_username || null,
            photo: userDetail.photo || null,
            part_time: userDetail.part_time || null,
            profiles: userDetail.profiles.slice() || [],
            start_date: userDetail.start_date || null,
            inactive_date: userDetail.inactive_date || null,
            status: userDetail.status || false,
            network_profiles: userDetail.network_profiles.slice() || [],
            contract: userDetail.contract || null,
            category: userDetail.category || null

        } || {};
    },

    editUserForm(userForm) {
        let numberString = userForm.phone_numbers.reduce((arr, phone) => arr.concat(phone.prefix + phone.number + ','), '');
        let phoneNumbers = numberString.substr(0, numberString.length - 1);
        delete userForm.tel;
        return { ...userForm, phone_numbers: phoneNumbers };
    },

    manglePhoneNumber(number) {
        var prefix = '';
        var num = '';

        number = number.replace(/ +/g, '');
        if (!number.match(/^[+\d]?[\d]+$/)) {
            return undefined;
        }

        if (number.length > 9 && (this.startsWith(number, '00') || this.startsWith(number, '+'))) {
            prefix = number.slice(0, number.length - 9);
            num = number.replace(/ +/g, '').slice(-9);
        } else {
            num = number;
        }

        return {
            'prefix': prefix,
            'number': num
        };
    },

    startsWith(str1, str2) {
        return str1.slice(0, str2.length) === str2;
    },


    shortName(name) {
        const nameParts = name.trim().split(' ');
        return nameParts[0] + (nameParts.length > 1 ? ' ' + nameParts[nameParts.length - 1] : '');
    },

    capitalizeFirstChar(str) {
        return str.charAt(0).toUpperCase() + str.slice(1);
    }
};

export default service;

export class IntraEvent {
    constructor(event) {
        this.date = event.date;
        this.event_arguments = event.event_arguments;
        this.pk = event.pk;
        this.project_unix_username = event.project_unix_username;
        this.time_since = event.time_since;
        this.type = event.type;
        this.message = event.message;
        this.decodedMessage = [];
    }

    transformEventMessage() {
        let strings = i18nService.getText(this.message).split("$");
        let j = 2;
        strings.forEach(str => {
            if (j % 2 === 0) {
                this.decodedMessage.push(str);
            } else {
                var aux = {};

                if (this.event_arguments[str].link !== undefined) {
                    aux["href"] = `/${this.event_arguments[str].link.substring(
                        1
                    )}`;
                }

                if (this.event_arguments[str].translate !== undefined) {
                    aux["value"] = i18nService.getText(
                        this.event_arguments[str].translate
                    );
                } else {
                    aux["value"] = this.event_arguments[str].value;
                }

                if (aux.href === undefined) {
                    aux["value"] = aux.value;
                }

                this.decodedMessage.push(aux);
            }
            j++;
        });
        return this;
    }
}
