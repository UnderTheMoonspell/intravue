import * as moment from "moment";

import appConfig from '@/app.config';

export const WEEK = {
    SUNDAY: 0,
    MONDAY: 1,
    TUESDAY: 2,
    WEDNESDAY: 3,
    THURSDAY: 4,
    FRIDAY: 5,
    SATURDAY: 6
};

/*
 * USE new Date().getDay() OR moment().day()
 * DO NOT USE moment().weekday(), the returned value depends on selected locale: moment.locale('pt')
 */

export const WEEKEND = [WEEK.SATURDAY, WEEK.SUNDAY];

const service = {

    displayFormat(date) {
        return moment(date).format(appConfig.calendar.display_format);
    },

    createListBetweenDates(start, end) {
        let dates = [];
        var date = moment(start, appConfig.calendar.api_format);
        while (date <= moment(end, appConfig.calendar.api_format)) {
            dates.push(date.format(appConfig.calendar.api_format));
            date = date.add(1, 'days');
        }
        return dates;
    },

    isDayHoliday(date, holidays) {
        // TODO
        return date && holidays;
    },

    isDayWeekend(date) {
        return WEEKEND.includes(moment(date).day());
    },

    formatGroupToString(group) {
        // Given a group of vacation days, formats them to a string that represents the interval
        return this.displayFormat(group[0][appConfig.calendar.api_key]) + (group.length === 1 ? '' : (' a ' + this.displayFormat(group[group.length - 1][appConfig.calendar.api_key])));
    },

    removeWeekendsAndHolidays(dates, key) {
        /*
         * Given an array of dates, removes the days that are weekends on holidays
         * Use the key to target an property if the dates items are objects
         */
        // return dates.filter(date => !this.isDayWeekend(key ? date[key] : date) && !this.isDayHoliday(key ? date[key] : date));
        return dates.filter(date => !this.isDayWeekend(key ? date[key] : date));
    },

    groupMultipleUsersVacations(vacations) {
        /**
         * Given an array with vacations of multiple users, returns an object where each key is a user's unix_username
         * and the value is their vacations
         */
        return vacations.reduce((acc, value) => {
            // Group by user
            if (!acc[value.user_unix_username]) acc[value.user_unix_username] = [];
            acc[value.user_unix_username].push(value);
            return acc;
        }, {});
    },

    dateIntervalToDates(start, end) {
        let first = moment.utc(start);
        let dates = [first.toDate()];
        while (!first.isSame(moment.utc(end), 'day')) {
            var newDate = first.clone().add(1, 'days');
            dates.push(newDate.toDate());
            first = newDate;
        }
        return dates;
    },

    groupedVacations(vacations, holidays) {
        let mappedHolidays = (holidays || []).map(holiday => {
            return { [appConfig.calendar.api_key]: holiday.date, holiday: true };
        });
        // The holidays are added to the list of vacation days in order to calculate the contiguous vacation periods
        let groupedVacations = vacations.concat(mappedHolidays)
            .sort((a, b) => {
                return new Date(a[appConfig.calendar.api_key]).getTime() - new Date(b[appConfig.calendar.api_key]).getTime();
            })
            .reduce((groups, curr, index) => {
                const dayInMilliseconds = 1000 * 3600 * 24;
                let currGroup = null;
                const date = new Date(curr[appConfig.calendar.api_key]);
                let arr;
                if (!groups.length) {
                    arr = [];
                    groups.push(arr);
                }
                if (!index) groups[0].push(curr);
                else {
                    currGroup = groups[groups.length - 1];
                    let lastInserted = currGroup[currGroup.length - 1];
                    let datesDifference = date.getTime() - new Date(lastInserted[appConfig.calendar.api_key]).getTime();
                    if (datesDifference <= dayInMilliseconds) currGroup.push(curr);
                    else {
                        arr = [curr];
                        groups.push(arr);
                    }
                }
                // groupWithWeekendsAndHolidays=true
                if (moment(date).day() === WEEK.FRIDAY) {
                    currGroup = groups[groups.length - 1];
                    // Also add the weekend if the current day is friday
                    let saturday = { weekend: true, [appConfig.calendar.api_key]: new Date(date.getTime() + dayInMilliseconds) };
                    let sunday = { weekend: true, [appConfig.calendar.api_key]: new Date(date.getTime() + dayInMilliseconds * 2) };
                    currGroup.push(saturday, sunday);
                }
                return groups;
            }, [])
            .filter(group => {
                // Sanitize the absence groups, by removing groups that do not include a user vacation
                const isHolidayOnlyGroup = group.length === 1 && group[0].holiday;
                const isHolidayWeekendOnlyGroup = !group.filter(day => !day.weekend && !day.holiday).length;
                return !isHolidayOnlyGroup && !isHolidayWeekendOnlyGroup;
            })
            .map(group => {
                // A vacation group cant end on an holiday or a weekend
                let lastElement = group[group.length - 1];
                while (lastElement.weekend || lastElement.holiday) {
                    group.pop();
                    lastElement = group[group.length - 1];
                }
                return group;
            });
        return groupedVacations;
    }
};

export default service;