export default {
    inserted: (el, binding) => {
        window.addEventListener("click", e => {
            let drawer1 = document.getElementById(binding.value.drawers[0]);
            let drawer2 = document.getElementById(binding.value.drawers[1]);
            if ((drawer1 && drawer1.contains(e.target)) || (drawer2 && drawer2.contains(e.target))) {
                return;
            }
            if (
                !el.contains(e.target) &&
                (!e.target.closest("li") ||
                    binding.value.elementException.indexOf(
                        e.target.closest("li").getAttribute("id")
                    ) < 0)
            ) {
                binding.value.method(e);
            }
        });
    }
};
