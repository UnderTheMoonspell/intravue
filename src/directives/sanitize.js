export default {
    bind(el, binding) {
        el.innerHTML = binding.value.replace(/<script>/g, '');
    }
};
