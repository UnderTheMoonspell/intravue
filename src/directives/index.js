import clickOutside from './click-outside';
import SanitizeHTML from '../directives/sanitize';

export default {
    'click-outside': clickOutside,
    'sanitize': SanitizeHTML

}