
const path = require('path');

module.exports = {
  css: {
    loaderOptions: {
      sass: {
        includePaths: [
          path.resolve(__dirname, './node_modules/compass-mixins/lib')
        ]
      }
      // scss: {
      //   // indentedSyntax: true,
      //   includePaths: [
      //     path.resolve(__dirname, './node_modules/compass-mixins/lib')
      //   ]
      // }
    }
  },
  devServer: {
    disableHostCheck: true
  },
  chainWebpack: config => {
    config.module.rule('eslint').use('eslint-loader').options({
      fix: true
    })

    config.plugins.delete('prefetch')
  }
}

