module.exports = {
  root: true,
  env: {
    node: true
  },
  extends: ["plugin:vue/essential", "@vue/standard"],
  plugins: ["vue"],
  rules: {
    "no-console": process.env.NODE_ENV === "production" ? "error" : "off",
    "no-debugger": process.env.NODE_ENV === "production" ? "error" : "off",
    indent: "off",
    semi: "off",
    "space-before-function-paren": "off",
    "no-multiple-empty-lines": "off",
    "eol-last": "off",
    quotes: "off",
    "no-useless-computed-key": "off"
  },
  parserOptions: {
    parser: "babel-eslint"
  }
};
